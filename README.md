


# CORE Education Trust V1.0

A bespoke WordPress theme created for CORE Education Trust, by Glass

* [View live](https://core-education.co.uk)
* View staging (coming soon)
* [Author - Glass](https://www.designby.glass)



## Table of Contents

* [About the Project](#markdown-header-about-the-project)
  * [Built With](#markdown-header-built-with)
* [Getting Started](#markdown-header-getting-started)
  * [Prerequisites](#markdown-header-prerequisites)
  * [Installation](#markdown-header-installation)
  * [Working & Deployment](#markdown-header-working-&-deployment)
* [Approach & Structure](#markdown-header-approach-and-structure)
* [Contributing](#markdown-header-contributing)
* [Contact](#markdown-header-contact)
* [Acknowledgements](#markdown-header-acknowledgements)




## About The Project



### Built With
* [Wordpress](https://wordpress.org/)
* [Advanced custom fields (ACF)](https://advancedcustomfields.com)
* [JQuery](https://jquery.com)
* [SASS](https://wordpress.org/)




## Getting Started

This should be everything you need to get started, but there might be local conflicts.


### Prerequisites

As a base, you'll need to be comfortable in terminal, and have NPM installed.
* npm
```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo
```sh
git clone git@bitbucket.org:ryandc/core-education-trust.git
```
2. Install NPM packages
```sh
npm install
```
3. When you're ready to develop, run gulp to compile JS and CSS. This runs and then watches for changes in the src css + js folders.
```sh
gulp
```

### Working & deployment

Make sure to compile files locally, as this is not completed by a build process. Once you have access to the repo, Ryan has added your key to Bitbucket and you can commit changes (use Tower on OSX if you're not cool with Terminal), make sure to switch to switch to the develop branch.

Pushing to the develop branch will automatically deploy to the staging site, and deploying to master will automatically deploy to the live site.

This is done via DeployHQ (nice and easy…)

Any additional JS libraries or plugins should be added via NPM, and then added to the build process via the jsTask() in gulpfile.js. Then simply add a new js file in the src/js/modules folder to write your JS.


## Approach and structure

The website is hosted on a SimplyCloud VPS using Plesk.

WordPress is setup as a multi-site network, with CORE being the primary site. Other school sites will be added in the future.

The website has been designed and built loosely following the Atomic & BEM (Block Element Modifier) methodologies.

You’ll find everything you need either in the main directory, src/, or template-parts.

The latter folder contains re-usable page sections, smaller elements and the page-builder loops.

The site is heavily built with ACF Flexible content fields. These are used to create most pages, out of a collection of defined and customisable ‘blocks’.

The ACF fields have been abstracted out in some cases, to cut down on duplication. Many blocks which feature static text, can be overridden using the Options menu in the WP admin.

Any changes to the ACF fields are saved automatically to a JSON file in the theme folder. After deploying, you’ll need to go into the Custom Fields section in the WP admin to sync the groups which have changed.


## Contributing

If you are working on the project and need access and to deploy to the server, contact Ryan to get permission.

If you just edit via FTP, any future deployments from the repo will override any changes. ALWAYS work within the repo.



## Contact

Ryan Dean-Corke - [@ryandc](https://twitter.com/ryandc) - ryan@designby.glass




## Acknowledgements
* [Brand by Luke Tonge](http://www.luketonge.com/)
* [LazySizes](https://github.com/aFarkas/lazysizes)
* [Headroom](https://github.com/WickyNilliams/headroom.js)
* [Sticky Sidebar](https://abouolia.github.io/sticky-sidebar/)
* [Appear on Scroll](https://michalsnik.github.io/aos/)
* [FitVids](http://fitvidsjs.com/)
* [Swiper](https://swiperjs.com/)
* [DeployHQ](https://www.deployhq.com/)



