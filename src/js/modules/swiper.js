
$('.home-slider').each(function(){
    new Swiper($(this), {
        autoplay: {
            delay: 4500,
            disableOnInteraction: true,
        },
        effect: 'fade',
        simulateTouch: false,
        navigation: {
            nextEl: '.home-slider__next',
            prevEl: '.home-slider__prev',
        },
    });
});


$('.quote-slider').each(function(){
    new Swiper($(this), {
        spaceBetween: 40,
        autoplay: {
            delay: 3000,
            disableOnInteraction: true,
        },
        breakpoints: {

            320: {
                slidesPerView: 1,
                slidesPerGroup: 1
            },
     
            1000: {
                slidesPerView: 2,
                slidesPerGroup: 1
            },
        }

    });
});


$('.quote-slider-two').each(function(){
    new Swiper($(this), {
        spaceBetween: 40,
        autoplay: {
            delay: 3000,
            disableOnInteraction: true,
        },
        breakpoints: {

            320: {
                slidesPerView: 1,
                slidesPerGroup: 1
            },
     
            900: {
                slidesPerView: 2,
                slidesPerGroup: 1
            },

            1200: {
                slidesPerView: 3,
                slidesPerGroup: 1
            },

        }

    });
});


$(document).ready(function() { 
$(window).on('load', function() {

    $('.twitter-block__list').each(function(){
        new Swiper($(this), {
            spaceBetween: 30,
            slidesPerView: 'auto',
            wrapperClass: 'tweets',
            slideClass: 'tweet',
            navigation: {
                nextEl: '.twitter-block__next',
                prevEl: '.twitter-block__prev',
            },
            speed:450,
            autoplay: {
                delay: 3000,
            },
        });
    });

});
});

