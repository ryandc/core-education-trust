

var $allMenuItems = $('.header__nav-primary > ul > li.has-sub-menu');
var $allMobMenuItems = $('.mobile-menu__links > ul > li.has-sub-menu');
    

  //Adds menu-mobile class (for mobile toggle menu) before the normal menu
  //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
  //Normal menu is hidden if width is below 959px, and jquery adds mobile menu


  if ($(window).width() > 899) {

    var timerMenu;

    $(".header__nav-primary > ul > li.has-sub-menu").on("mouseover", function() {
      var that = $(this);
      clearTimeout(timerMenu);
      
        if ( $allMenuItems.hasClass('sub-menu-active') ) {
            $allMenuItems.removeClass('sub-menu-active');
            openSubmenu(that);
        } else {
            openSubmenu(that);
        }

    }).on("mouseleave", function() {
        var that = $(this);

        timerMenu = setTimeout( function(){
          closeSubmenu(that)
      }, 300);
    });

    function openSubmenu(thisObj) {
     thisObj.addClass("sub-menu-active");
    }
    function closeSubmenu(thisObj) {
     thisObj.removeClass("sub-menu-active");
    }

  }



  
  //If width is more than 943px dropdowns are displayed on hover
if ($(window).width() < 890) {

  $(".mobile-menu__links > ul > li.has-sub-menu").click(function(e) {
        
        if ( $(this).hasClass('sub-menu-active') ) {

            $(this).removeClass('sub-menu-active');

        } else if ( $allMobMenuItems.hasClass('sub-menu-active') ) {

            $allMobMenuItems.removeClass('sub-menu-active');
            $(this).addClass('sub-menu-active');

        } else {

            $(this).addClass('sub-menu-active');
            
        }
  });

}


