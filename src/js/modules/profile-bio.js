
// Side menu functions
var core_profile = {

    cache: function() {
        core = {};
        core.people = $('.people__item'),
        core.menuTriggerOpen = $('a.people__item'),
        core.menuTriggerClose = $('.people__bio-close, .bg-overlay'),
        core.pagebody = $('body');
    },

    on_ready: function() {
        core_profile.cache();
        core_profile.bindMenuOpen();
        core_profile.bindMenuClose();

    },

    bindMenuOpen: function() {
        core.menuTriggerOpen.on("click", function(event) {

            event.preventDefault();
            core.pagebody.addClass('overlay-active');
            $(this).next().addClass('active');

        });

    },

    bindMenuClose: function() {
        core.menuTriggerClose.on("click", function(event) {

            event.preventDefault();
            core.people.next().removeClass('active');
            core.pagebody.removeClass('overlay-active');

        });

    },


}

$(document).ready( core_profile.on_ready );

