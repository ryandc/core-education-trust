
// grab an element
var element = document.querySelector("body");
// options
var options = {
    // vertical offset in px before element is first unpinned
    offset : 52
};


if(Headroom.cutsTheMustard) {
  var hr = new Headroom(element, options);
  hr.init();
}