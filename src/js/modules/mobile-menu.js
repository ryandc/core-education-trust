
// Side menu functions
var core_sub_menu = {

    cache: function() {

        menuSubTrigger = $('.mobile-menu__links .mobile-menu__sub-trigger');
    },

    on_ready: function() {
        core_sub_menu.cache();
        core_sub_menu.bindSubMenuOpen();

    },

    bindSubMenuOpen: function() {
        menuSubTrigger.on("click", function(event) {
            event.preventDefault()
            $(this).parents('li').toggleClass('sub-menu-active');

        });

    },
}

$(document).ready( core_sub_menu.on_ready );



// Side menu functions
var tcr_side_menu = {

    cache: function() {
        tcr = {};
        tcr.menuTrigger = $('.header__menu-btn__trigger, .mobile-menu__menu-btn__trigger, .mobile-menu__overlay'),
        tcr.pagebody = $('body');
    },

    on_ready: function() {
        tcr_side_menu.cache();
        tcr_side_menu.bindMenuOpen();

    },

    bindMenuOpen: function() {
        tcr.menuTrigger.on("click", function(event) {

            event.preventDefault()
            tcr.pagebody.toggleClass('menu-active');

        });

    },


}

$(document).ready( tcr_side_menu.on_ready );
