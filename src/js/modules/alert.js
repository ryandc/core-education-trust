// Message alert

const alertBanner = document.querySelector('.site-wide-message');
const noAlert = document.querySelector('.no-message');

if(alertBanner) {

    const cookieSet = document.querySelector('.site-wide-message__hide');
    cookieSet.addEventListener('click', e => {
        e.preventDefault();
        setCookie('hideAlertBanner', 'yes', 7);
        alertBanner.classList.add('hide-it');
    })

} else if (noAlert) {
    deleteCookie('hideAlertBanner');
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function deleteCookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
