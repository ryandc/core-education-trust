<?php
get_header();
get_template_part( 'template-parts/page-sections/page-heading' );
get_template_part( 'template-parts/page-sections/page-image' ); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<main role="main" id="content">

			<div class="wrapper wrapper--offset">
				<section class="section section--sm">
					<div class="row">
						<div class="col-xs-12 col-lg-9 mb-lg">
							<?php the_field( 'project_info' ); ?>
						</div>
						<div class="col-xs-12 col-lg-3">				
							<a href="<?php the_field( 'website_url' ); ?>" target="_blank" class="btn btn--wide">Find out more</a>
						</div>
					</div>
				</section>

				<hr>	

				<?php if ( have_rows( 'project_partners' ) ) : ?>
					<?php while ( have_rows( 'project_partners' ) ) : the_row(); ?>
						<section class="section--sm">
							<h2 class="heading-4 mb-lg"><?php the_sub_field( 'partner_type_heading' ); ?></h2>
							<div class="project-partners row middle-xs">
								<?php if ( have_rows( 'partner_logos' ) ) : ?>
									<?php while ( have_rows( 'partner_logos' ) ) : the_row(); ?>
										<?php $partner_logo = get_sub_field( 'partner_logo' ); ?>
										<div class="col-xs-6 col-md-4 col-lg-3 tc mb-lg">
								 			<?php echo wp_get_attachment_image( $partner_logo, 'full' ); ?>
								 		</div>
									<?php endwhile; ?>
								<?php else : ?>
									<?php // no rows found ?>
								<?php endif; ?>
							</div>
						</section>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>



			</div>


	</main>


    <?php

    $currentID = get_the_ID();

    $args = array(
        'post_type'              => 'coreandco',
        'posts_per_page'         => '1',
        'post__not_in' => array($currentID),
        'order'                  => 'ASC',
		'orderby'                => 'rand'

    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) { ?>

		<div class="wrapper">
			<hr>	
		</div>

		<section class="more-jobs section">
			<div class="wrapper wrapper--offset mb-xxl">
				<h2 class="heading-long-statement">Explore another project</h2>	
			</div>
			<div class="wrapper">
				

		        <?php while ( $query->have_posts() ) {
		            $query->the_post(); ?>

		        
		        	<?php get_template_part( 'template-parts/page-elements/block-project' ); ?>

		        <?php } ?>

			</div>
		</section>

    <?php } 
    // Restore original Post Data
    wp_reset_postdata();
    ?>



	<?php endwhile; endif; ?>


<?php get_footer(); ?>