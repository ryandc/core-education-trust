<?php
/*
 Template Name: 🚫 Contact
*/
get_header();
get_template_part( 'template-parts/page-sections/page-heading' );
get_template_part( 'template-parts/page-sections/page-intro' );
get_template_part( 'template-parts/page-sections/page-image' ); ?>

		<main role="main" id="content">
<!-- 
			<div class="wrapper">
				<hr class="bg--grad-purple" />
			</div> -->	

			<div class="wrapper mb-xl">
				<div class="layout wrapper--z">
					<div class="sidebar sidebar--layout">
						<div class="sidebar__inner">
							<div class="section-content">
								<div class="sidebar__block">
									<h3 class="heading-4"><?php the_field( 'address_label' ); ?></h3>
									<div class="mb"><?php the_field( 'address' ); ?></div>

									<?php $map_graphic = get_field( 'map_graphic' ); ?>
									<?php if ( $map_graphic ) { ?>
										<a href="<?php the_field( 'google_maps_link' ); ?>" class="db">
											<?php echo wp_get_attachment_image( $map_graphic, 'full' ); ?>
										</a>
									<?php } ?>
								</div>			

								<div class="sidebar__block">
									<h3 class="heading-4"><?php the_field( 'social_label' ); ?></h3>
									<ul class="list-inline social-icons social-icons--left mt ">
										<?php get_template_part( 'template-parts/page-elements/social-media' ); ?>
									</ul>
								</div>
							</div>
													
						</div>
					</div>

					<div class="main">

						<?php get_template_part( 'template-parts/page-builder-simple' ); ?>
						
						<div class="sidebar sidebar--mobile">
							<hr class="section--sm bg--grad-orange" />

							<div class="sidebar__block">
								<h3 class="heading-4"><?php the_field( 'address_label' ); ?></h3>
								<div class="mb"><?php the_field( 'address' ); ?></div>

								<?php $map_graphic = get_field( 'map_graphic' ); ?>
								<?php if ( $map_graphic ) { ?>
									<a href="<?php the_field( 'google_maps_link' ); ?>" class="db">
										<?php echo wp_get_attachment_image( $map_graphic, 'full' ); ?>
									</a>
								<?php } ?>
							</div>			

							<div class="sidebar__block">
								<h3 class="heading-4"><?php the_field( 'social_label' ); ?></h3>
								<ul class="list-inline social-icons social-icons--left mt ">
									<?php get_template_part( 'template-parts/page-elements/social-media' ); ?>
								</ul>
							</div>
						</div>

	
					</div>
				</div>
			</div>

		</main>

			<?php if ( have_rows( 'page_links' ) ) : ?>
	
				<div class="page-link-blocks <?php the_field( 'choose_a_gradient_type' ); ?>">
					<div class="row center-xs">	
						<?php while ( have_rows( 'page_links' ) ) : the_row(); ?>
							<?php $post_object = get_sub_field( 'page' ); ?>
							<?php if ( $post_object ): ?>
								<?php $post = $post_object; ?>
								<?php setup_postdata( $post ); ?> 

									<div class="col-xs-12 col-md-6 col-mdlg mb-grid">
										<?php get_template_part( 'template-parts/page-elements/block-page-links' ); ?>
									</div>

								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
				
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
	<div class="bg--shape-fixed">
		<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst-white" /></svg>
	</div>
<?php get_footer(); ?>