<?php

/**
 * Page pagination
 */

function page_pagi() {
  global $wp_query;
  $bignum = 999999999;
  if ( $wp_query->max_num_pages <= 1 )
    return;
  echo '<div class="section"><div class="wrapper"><div class="pagination">';
  echo paginate_links( array(
    'base'         => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
    'format'       => '',
    'current'      => max( 1, get_query_var('paged') ),
    'total'        => $wp_query->max_num_pages,
    'prev_text'    => '<div class="pagination__arrow pagination__arrow-prev"><svg viewBox="0 0 24 24" class=""><use xlink:href="#i-link-page" /></svg></div>',
    'next_text'    => '<div class="pagination__arrow pagination__arrow-next"><svg viewBox="0 0 24 24" class=""><use xlink:href="#i-link-page" /></svg></div>',
    'type'         => 'list',
    'end_size'     => 3,
    'mid_size'     => 3
  ) );
  echo '</div></div></div>';
} ?>