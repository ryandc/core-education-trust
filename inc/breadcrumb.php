<?php

add_filter( 'wpseo_breadcrumb_links', 'override_yoast_breadcrumb_trail' );

function override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular('people') || is_singular('foundation') || is_singular('school') || is_singular('roles_key') || is_singular('coreandco') ) {

        $fullSlug = str_replace(home_url(),'',get_post_permalink($post->ID));
        $fullSlugComponents = explode('/', $fullSlug);
        $crumbs = [];

        foreach ($fullSlugComponents as $index => $uri) {
            if ($uri == '' || $uri == $post->post_name) continue;

            $uriParts = array_slice($fullSlugComponents, 0, ($index + 1));
            $uriTest = implode('/', $uriParts);

            $crumbPost = url_to_postid($uriTest);

            if ($crumbPost && !in_array($crumbPost, get_post_ancestors($post->ID))) {

                $crumbs[] = array(
                    'id' => $crumbPost
                );
            }

        }

        array_splice( $links, 1, 0, $crumbs );

    }

    return $links;
}