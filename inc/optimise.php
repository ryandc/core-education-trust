<?php





add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );
/*
 * Modify TinyMCE editor to remove H1.
 */
function tiny_mce_remove_unused_formats($init) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4';
	return $init;
}




/**
 * Remove sub nav entries
 * @return null
 * @codeCoverageIgnore
 */
if (! function_exists('menu_page_removing')) {
	function menu_page_removing() {
		//remove_menu_page( 'edit.php?post_type=acf-field-group' );  
		remove_menu_page( 'edit-comments.php' );  
		remove_menu_page( 'widgets.php' );  
		remove_menu_page( 'link-manager.php' );  
	}
	add_action( 'admin_menu', 'menu_page_removing',999 );
}


/**
 * Remove sub nav entries
 * @return null
 * @codeCoverageIgnore
 */
if (! function_exists('menu_subpage_removing')) {
	function menu_subpage_removing() {
		remove_submenu_page('themes.php', 'widgets.php');
		remove_submenu_page('themes.php', 'theme-editor.php');
		remove_submenu_page('themes.php', 'themes.php');

		// remove_submenu_page('options-general.php', 'options-media.php');
		// remove_submenu_page('options-general.php', 'options-permalink.php');
		remove_submenu_page('options-general.php', 'options-discussion.php');
		// remove_submenu_page('index.php', 'update-core.php');
	}
	add_action( 'admin_menu', 'menu_subpage_removing',999 );
}


/**
 * Remove dashboard widgets
 * @return null
 * @codeCoverageIgnore
 */
if (! function_exists('admin_remove_dashboard_widgets')) {
	function admin_remove_dashboard_widgets() {
	    global $wp_meta_boxes;
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	}
	add_action('wp_dashboard_setup', 'admin_remove_dashboard_widgets');
}


/**
 * Hide the WordPress menu in the top left corner
 * @return null
 * @codeCoverageIgnore
 */
if(! function_exists('admin_remove_wordpress_menu')) {
	function admin_remove_wordpress_menu() {
	   echo '<style type="text/css">#wp-admin-bar-wp-logo { display: none; }</style>';
	}
	add_action('admin_head', 'admin_remove_wordpress_menu');
}


/**
 * Change the WordPress logo above the login box
 * @return null
 * @codeCoverageIgnore
 */
if(! function_exists('admin_login_logo')) {
	function admin_login_logo() {
	    echo '<style type="text/css"> .login h1 a {
	    	background-image: url('. get_bloginfo('template_directory') .'/dist/imgs/vertical-core-logo.svg);
			background-size: 155px;
			height: 156px;
			width: 155px;
	    }
	    .login form {
	    	box-shadow:none;
			border: none;
			border-radius: 0px;
	    }
	    body {
	    	background-color:#FFF;
	    }</style>';
	}
	add_action('login_head', 'admin_login_logo');
}


/**
 * Change the logo link from wordpress.org
 * @return null
 * @codeCoverageIgnore
 */
if(! function_exists('admin_login_url')) {
	function admin_login_url() {
		return '';
	}
	add_filter('login_headerurl', 'admin_login_url');
}


/**
 * Change the alt text on the logo
 * @return null
 * @codeCoverageIgnore
 */
if(! function_exists('admin_login_title')) {
	function admin_login_title() {
		return '';
	}
	add_filter('login_headertext', 'admin_login_title');
}


/**
 * Change the admin footer message
 * @return null
 * @codeCoverageIgnore
 */
if(! function_exists('admin_change_footer_admin')) {
	function admin_change_footer_admin () {
	    echo '';
	}
	add_filter('admin_footer_text', 'admin_change_footer_admin');
}





function setup_theme(  ) {
    // Theme setup code...
    
    // Filters the oEmbed process to run the responsive_embed() function
    add_filter('embed_oembed_html', 'responsive_embed', 10, 3);
}
add_action('after_setup_theme', 'setup_theme');
/**
 * Adds a responsive embed wrapper around oEmbed content
 * @param  string $html The oEmbed markup
 * @param  string $url  The URL being embedded
 * @param  array  $attr An array of attributes
 * @return string       Updated embed markup
 */
function responsive_embed($html, $url, $attr) {
    return $html!=='' ? '<div class="embed-container">'.$html.'</div>' : '';
}




/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

/**
 * Move Yoast to bottom
 */
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');



/**
 * Remove Guttenberg block ref
 */
function remove_block_css(){
wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );


?>