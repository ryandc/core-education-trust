<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
	
	<div class="wrapper wrapper--offset">
		<h3 class="heading-5 mb-xl"><?php echo $query->found_posts; ?> roles found</h3>
	</div>
<div class="wrapper">
	<div class="mb-xl">
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
		<?php get_template_part( 'template-parts/page-elements/block-role' ); ?>
		<?php
	}
	?>

	</div>

	
	<div class="pagination tc">

		<h5>Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?></h5>

		<div class="job-role-pagination center-xs">
			
			<div class="nav-previous"><?php next_posts_link( '<svg viewBox="0 0 24 24" class=""><use xlink:href="#i-link-page" /></svg>', $query->max_num_pages ); ?></div>
			<div class="nav-next"><?php previous_posts_link( '<svg viewBox="0 0 24 24" class=""><use xlink:href="#i-link-page" /></svg>' ); ?></div>

		</div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>