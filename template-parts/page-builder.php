
<?php if ( have_rows( 'builder-page' ) ): ?>
	<?php while ( have_rows( 'builder-page' ) ) : the_row(); ?>


		<?php if ( get_row_layout() == 'text_+_image' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/text-image' ); ?>


		<?php elseif ( get_row_layout() == 'simple_text_content' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/simple-text' ); ?>



		<?php elseif ( get_row_layout() == 'page_link_blocks' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/page-link-blocks' ); ?>


		<?php elseif ( get_row_layout() == 'images' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/images' ); ?>



		<?php elseif ( get_row_layout() == 'quotes' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/quotes' ); ?>


		<?php elseif ( get_row_layout() == 'people' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/people' ); ?>



		<?php elseif ( get_row_layout() == 'timeline' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/timeline' ); ?>



		<?php elseif ( get_row_layout() == 'divider' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/divider' ); ?>



		<?php elseif ( get_row_layout() == 'text_columns' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/text-columns' ); ?>



		<?php elseif ( get_row_layout() == 'our_schools' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/our-schools' ); ?>



		<?php elseif ( get_row_layout() == 'showcase_latest' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/showcase-latest' ); ?>



		<?php elseif ( get_row_layout() == 'list_all' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/list-all-content' ); ?>

		<?php elseif ( get_row_layout() == 'code' ) : ?>

			<?php get_template_part( 'template-parts/page-builder-sections/code' ); ?>




		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>