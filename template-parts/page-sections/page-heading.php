<?php if (
	is_page_template( 'template-top-level.php' ) ||
	is_page_template( 'template-contact.php' ) ||
	is_page_template( 'template-coreandco.php' )
) { ?>

	<section class="page-heading">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="100">

			<?php if( get_field('page_heading_override') ) { ?>
				<h1 class="heading-larger"><?php the_field('page_heading_override'); ?></h1>
			<?php } else { ?>
				<h1 class="heading-larger"><?php the_title(); ?></h1>
			<?php } ?>

		</div>
	</section>

<?php } elseif (
	is_single() && is_post_type('school')
) { ?>

	<section class="page-heading">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="100">

			<a href="/schools/" class="mb page-heading__type">View all schools</a>
			<?php if( get_field('page_heading_override') ) { ?>
				<h1 class="heading-larger"><?php the_field('page_heading_override'); ?></h1>
			<?php } else { ?>
				<h1 class="heading-larger"><?php the_title(); ?></h1>
			<?php } ?>

		</div>
	</section>



<?php } elseif (
	is_single() && is_post_type('coreandco')
) { ?>

	<section class="page-heading">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="100">

			<a href="/core-co-foundation/" class="mb page-heading__type">View all projects</a>
			<h1 class="heading-larger"><?php the_title(); ?></h1>
			
		</div>
	</section>

	<section class="page-intro">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="120">
			<p class="copy-lg"><?php the_field( 'page_intro_text' ); ?></p>
		</div>

	</section>




<?php } elseif (
	is_single() && is_post_type('roles_key')
) { ?>

	<section class="page-heading">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="100">

			<a href="/work-with-us" class="mb page-heading__type">View all roles</a>
			<h1 class="heading-long-statement"><?php the_title(); ?></h1>
			
		</div>
	</section>



<?php } elseif (
	is_page() && !is_page_template()
) { ?>

	<section class="page-heading">
		<div class="wrapper wrapper--z">
			<div class="layout layout--end">
				<div class="main">
					<div class="wrapper--small" data-aos="fade-left" data-aos-delay="100">
						<?php if( get_field('page_heading_override') ) { ?>
							<h1><?php the_field('page_heading_override'); ?></h1>
						<?php } else { ?>
							<h1><?php the_title(); ?></h1>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php } ?>


