<?php $current_page_id = get_the_ID(); ?>

        <?php if ( have_rows( 'menu_items', 'option' ) ) : ?>
            <?php while ( have_rows( 'menu_items', 'option' ) ) : the_row(); ?>

                <?php if ( get_sub_field('link_type') == 'link' ) { ?>

                    <?php $post_object = get_sub_field( 'what_page_should_this_link_to' ); ?>
                    <?php if ( $post_object ): ?>
                        <?php $post = $post_object; ?>
                        <?php setup_postdata( $post ); ?> 
                             <li class="<?php if ( get_the_ID() == $current_page_id ) { echo 'current'; } ?>"><a href="<?php the_permalink(); ?>"><?php the_sub_field( 'menu_label' ); ?></a></li>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                <?php } else { ?>

                    

                    <?php $post_object = get_sub_field( 'what_page_should_this_link_to' ); ?>
                    <?php if ( $post_object ): ?>
                        <?php $post = $post_object; ?>
                        <?php setup_postdata( $post ); ?> 

                        <li class="has-sub-menu <?php if ( get_the_ID() == $current_page_id ) { echo 'current'; } ?>">
                        <a href="<?php the_permalink(); ?>">
                            <?php the_sub_field( 'menu_label' ); ?>
                            <svg viewBox="0 0 29 29" class=""><use xlink:href="#i-chevron" /></svg>
                         </a>

                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>



                        <ul class="sub-menu">
                             <?php if ( have_rows( 'menu_columns' ) ): ?>
                                <?php while ( have_rows( 'menu_columns' ) ) : the_row(); ?>

                                    <?php if ( get_row_layout() == 'list_of_page_links' ) : ?>

                                        <?php $post_objects = get_sub_field( 'links' ); ?>
                                        <?php if ( $post_objects ): ?>
                                            <li><h3 class="heading-6"><?php the_sub_field( 'column_heading' ); ?></h6><ul class="list-bare">
                                            <?php foreach ( $post_objects as $post ):  ?>
                                                <?php setup_postdata( $post ); ?>
                                               <li class="<?php if ( get_the_ID() == $current_page_id ) { echo 'current'; } ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                            <?php endforeach; ?>
                                        </ul></li>
                                            <?php wp_reset_postdata(); ?>
                                        <?php endif; ?>

        
                                    <?php elseif ( get_row_layout() == 'image' ) : ?>

                                        <li class="sub-menu__image">
                                        <?php $image = get_sub_field( 'image' ); ?>
                                        <?php if ( $image ) { ?>
                                            <div class="image image--4x3">
                                                <?php
                                                    $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAYAAAC09K7GAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAABKADAAQAAAABAAAAAwAAAABUhAAoAAAADElEQVQIHWNgIBkAAAAzAAEVe/f+AAAAAElFTkSuQmCC";
                                                    $img_src_1 = wp_get_attachment_image_url( $image, '4x3-xs' );
                                                    $img_src_2 = wp_get_attachment_image_url( $image, '4x3-sm' );
                                                    $img_src_3 = wp_get_attachment_image_url( $image, '4x3-md' );
                                                    $img_src_4 = wp_get_attachment_image_url( $image, '4x3-lg' );
                                                    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>

                                                    <img
                                                        src="<?php echo $img_src_0; ?>"
                                                        data-srcset="
                                                            <?php echo $img_src_1; ?> 330w,
                                                            <?php echo $img_src_2; ?> 450w,
                                                            <?php echo $img_src_3; ?> 600w,
                                                            <?php echo $img_src_4; ?> 940w"
                                                        data-src="<?php echo $img_src_1; ?>"
                                                        data-sizes="auto"
                                                        class="lazyload"
                                                        alt="<?php echo $img_alt; ?>" />
                                            </div>
                                        <?php } ?>
                                        </li>

                                    <?php endif; ?>


                                <?php endwhile; ?>
                            <?php else: ?>
                                <?php // no layouts found ?>
                            <?php endif; ?>
                        </ul>
                    </li>

                <?php } ?>
                
            <?php endwhile; ?>
        <?php else : ?>
            <?php // no rows found ?>
        <?php endif; ?>

