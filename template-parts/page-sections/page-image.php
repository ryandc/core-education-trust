<?php if ( get_field( 'display_image' ) == 1 ) { ?>

	<?php if ( have_rows( 'header_image' ) ) : ?>
		<?php while ( have_rows( 'header_image' ) ) : the_row(); ?>

			<?php if ( get_sub_field( 'type_of_header_image' ) == 'landscape' ) { ?>

				<div class="header-image__landscape-shapes">

					<div class="wrapper wrapper--z">
		

					<?php if ( has_post_thumbnail() ) { ?>
				


					    <?php
					    $image = get_post_thumbnail_id();

			            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAAqADAAQAAAABAAAAAgAAAADO0J6QAAAAC0lEQVQIHWNgQAcAABIAAYAUyswAAAAASUVORK5CYII=";
			            $img_src_1 = wp_get_attachment_image_url( $image, '4x2-xs' );
			            $img_src_2 = wp_get_attachment_image_url( $image, '4x2-sm' );
			            $img_src_3 = wp_get_attachment_image_url( $image, '4x2-md' );
			            $img_src_4 = wp_get_attachment_image_url( $image, '4x2-lg' );
			            $img_src_5 = wp_get_attachment_image_url( $image, '4x2-xl' );
			            $img_src_6 = wp_get_attachment_image_url( $image, '4x2-xxl' );
			            $img_src_7 = wp_get_attachment_image_url( $image, '4x2-xxxl' );
					    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>

					    <?php if ( $image ) { ?>

					        <div class="image" data-aos="fade-up">
					        	
					            <img
			                        src="<?php echo $img_src_0; ?>"
			                        data-srcset="
			                            <?php echo $img_src_1; ?> 330w,
			                            <?php echo $img_src_2; ?> 450w,
			                            <?php echo $img_src_3; ?> 600w,
			                            <?php echo $img_src_4; ?> 940w,
			                            <?php echo $img_src_5; ?> 1200w,
			                            <?php echo $img_src_6; ?> 1800w,
			                            <?php echo $img_src_7; ?> 2400w"
			                        data-src="<?php echo $img_src_1; ?>"
			                        data-sizes="auto"
			                        class="lazyload"
					                alt="<?php echo $img_alt; ?>" />
					        </div>
					    <?php } ?>


					<?php } else { ?>
						<img src="https://source.unsplash.com/random/3000x1500" />
					<?php } ?>

					</div>

					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1395.08 738"><defs></defs><path d="M168.71 175.26C117.56 111.53 60.48 64.31 0 35.12V738h545.83C359 450.12 215.42 233.81 168.71 175.26zM1395.08 170.12a343.48 343.48 0 00-58.19-25.47c-90.11-29.75-191.46-19.63-285.41 28.52-95.58 49-161.32 123.36-190.13 214.92-32.08 102-15.21 219.7 48.81 340.4 1.54 2.89 3.29 6.07 5.21 9.51h479.71z"/></svg>
				</div>

			<?php } elseif ( get_sub_field( 'type_of_header_image' ) == 'maskcrop' ) { ?>

				<div class="header-image__mask-crop wrapper--z" data-aos="fade-up">

					<?php if ( has_post_thumbnail() ) { ?>
					    <?php
					    $image = get_post_thumbnail_id();

			            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAQCAYAAABk1z2tAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAKKADAAQAAAABAAAAEAAAAADIf7fRAAAAIUlEQVRIDe3QgQAAAADDoPlTH+SFUGHAgAEDBgwYMPA8MAoQAAHvBKviAAAAAElFTkSuQmCC";
			            $img_src_1 = wp_get_attachment_image_url( $image, 'header-mc-xs' );
			            $img_src_2 = wp_get_attachment_image_url( $image, 'header-mc-sm' );
			            $img_src_3 = wp_get_attachment_image_url( $image, 'header-mc-md' );
			            $img_src_4 = wp_get_attachment_image_url( $image, 'header-mc-lg' );
			            $img_src_5 = wp_get_attachment_image_url( $image, 'header-mc-xl' );
			            $img_src_6 = wp_get_attachment_image_url( $image, 'header-mc-xxl' );
			            $img_src_7 = wp_get_attachment_image_url( $image, 'header-mc-xxxl' );
					    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>

					    <?php if ( $image ) { ?>

					        <div class="image">
								<?php if ( get_sub_field( 'choose_a_gradient_type' ) !== 'bg--light' ) { ?>
									<div class="header-image__grad <?php the_sub_field( 'choose_a_gradient_type' ); ?>"></div>
								<?php } ?>
					            <img
			                        src="<?php echo $img_src_0; ?>"
			                        data-srcset="
			                            <?php echo $img_src_1; ?> 330w,
			                            <?php echo $img_src_2; ?> 450w,
			                            <?php echo $img_src_3; ?> 600w,
			                            <?php echo $img_src_4; ?> 940w,
			                            <?php echo $img_src_5; ?> 1200w,
			                            <?php echo $img_src_6; ?> 1800w,
			                            <?php echo $img_src_7; ?> 2400w"
			                        data-src="<?php echo $img_src_1; ?>"
			                        data-sizes="auto"
			                        class="lazyload"
					                alt="<?php echo $img_alt; ?>" />
					        </div>
					    <?php } ?>
					<?php } else { ?>
						<img src="https://source.unsplash.com/random/3000x1500" />
					<?php } ?>
					
				</div>

			<?php } elseif ( get_sub_field( 'type_of_header_image' ) == 'mask' ) { ?>

				<div class="header-image__mask wrapper--z" data-aos="fade-up">

					<?php if ( has_post_thumbnail() ) { ?>
					    <?php
					    $image = get_post_thumbnail_id();

			            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAOCAYAAAAvxDzwAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAADgAAAAC7RV3kAAAAFklEQVQ4EWNgGAWjITAaAqMhQI8QAAAEbgABix1U7wAAAABJRU5ErkJggg==";
			            $img_src_1 = wp_get_attachment_image_url( $image, 'header-m-xs' );
			            $img_src_2 = wp_get_attachment_image_url( $image, 'header-m-sm' );
			            $img_src_3 = wp_get_attachment_image_url( $image, 'header-m-md' );
			            $img_src_4 = wp_get_attachment_image_url( $image, 'header-m-lg' );
			            $img_src_5 = wp_get_attachment_image_url( $image, 'header-m-xl' );
			            $img_src_6 = wp_get_attachment_image_url( $image, 'header-m-xxl' );
			            $img_src_7 = wp_get_attachment_image_url( $image, 'header-m-xxxl' );
					    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>

					    <?php if ( $image ) { ?>

					        <div class="image">
								<?php if ( get_sub_field( 'choose_a_gradient_type' ) != 'bg--light' ) { ?>
									<div class="header-image__grad <?php the_sub_field( 'choose_a_gradient_type' ); ?>"></div>
								<?php } ?>
					            <img
			                        src="<?php echo $img_src_0; ?>"
			                        data-srcset="
			                            <?php echo $img_src_1; ?> 330w,
			                            <?php echo $img_src_2; ?> 450w,
			                            <?php echo $img_src_3; ?> 600w,
			                            <?php echo $img_src_4; ?> 940w,
			                            <?php echo $img_src_5; ?> 1200w,
			                            <?php echo $img_src_6; ?> 1800w,
			                            <?php echo $img_src_7; ?> 2400w"
			                        data-src="<?php echo $img_src_1; ?>"
			                        data-sizes="auto"
			                        class="lazyload"
					                alt="<?php echo $img_alt; ?>" />
					        </div>
					    <?php } ?>
					<?php } else { ?>
						<img src="https://source.unsplash.com/random/3000x1500" />
					<?php } ?>
					
				</div>

			<?php } elseif ( get_sub_field( 'type_of_header_image' ) == 'shape' ) { ?>

				<div class="header-image__shapes wrapper--z">
					
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1400.36 601.29"><defs><style>.cls-1{fill:#00ada1;}.cls-2{fill:#ffdb8f;}.cls-3{fill:#652f88;}.cls-4{fill:#e00069;}</style></defs><path d="M210.57 409.92c-26.1-21.7-53.32-35.9-80.35-42.14a136.34 136.34 0 0 0-44.5-3C53.6 368 24.26 382.88 0 407.94V601.3h403.87c-96.45-98.37-170.23-172.06-193.3-191.38z" class="cls-1"/><path d="M624.18 321.18c-25.8-34.5-56.6-56.48-89.6-64.1q-5.26-1.22-10.58-1.93c-33.22-4.53-66.44 5.2-96.1 28.1-29.16 22.5-47.43 53-51.46 85.82-4.56 37.16 8.7 76.56 38.33 113.94 12.86 16.17 53.36 59.36 110.08 118.3h271.6c-84.3-141.3-152.6-253.6-172.27-280.12z" class="cls-2"/><path d="M1067.92 164.5c-27.14-54.16-65.38-90.57-110.58-105.3q-6.36-2.08-12.85-3.58c-36.87-8.5-75.92-2.84-113.77 16.67-41.63 21.57-71.43 55.63-83.88 95.84-14.06 45.37-6.4 97.52 22.14 150.78 17.14 31.88 89.15 141 184.42 282.37h284.28c-81.3-217.5-149.8-396.4-169.78-436.8z" class="cls-3"/><path d="M1400.36 325c-19.37-114.6-35.43-204.66-41.68-227.56-9.5-34-27.28-59-51.4-72.4a86.3 86.3 0 0 0-22.55-8.66c-15.86-3.66-33-3-50.64 2.14-26.4 7.67-46.48 23.1-58.13 44.6-13 24.12-14.47 54.65-4.13 88.34 11.8 37.9 102.46 249.64 189.3 449.88h39.22z" class="cls-4"/></svg>

				</div>

			<?php } ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php } ?>

