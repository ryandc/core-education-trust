<?php if( get_field('page_intro_text') ) { ?>
	<section class="page-intro">
		<div class="wrapper wrapper--offset wrapper--z" data-aos="fade-left" data-aos-delay="120">
			<p class="copy-lg"><?php the_field( 'page_intro_text' ); ?></p>
		
			<?php if ( get_field( 'display_section_links_under_intro' ) == 1 ) { ?>
				<?php if ( have_rows( 'section_links' ) ) : ?>
					<?php while ( have_rows( 'section_links' ) ) : the_row(); ?>
						<?php if ( have_rows( 'links' ) ) : ?>
							<ul class="list-inline page-intro__section-links">
								<?php while ( have_rows( 'links' ) ) : the_row(); ?>
									<li><a href="#<?php the_sub_field( 'section_id' ); ?>"><?php the_sub_field( 'link_label' ); ?></a></li>
								<?php endwhile; ?>
							</ul>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			<?php } ?>
			
		</div>

	</section>
<?php } else { ?>
<!-- 	<section class="page-intro">
		<div class="wrapper wrapper--offset wrapper--z">
			<p class="copy-lg">THIS NEEDS TO BE ADDED FOR THIS PAGE CORE Education Trust’s mission is to provide children with a:</p>
		</div>
	</section> -->
<?php } ?>