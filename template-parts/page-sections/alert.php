<?php if ( get_field( 'turn_on_message_box', 'option' ) == 1 ) { ?>

    <?php if(isset($_COOKIE['hideAlertBanner']) == "yes" ){ }
     else { ?>

		<?php if ( have_rows( 'message', 'option' ) ) : ?>
			<?php while ( have_rows( 'message', 'option' ) ) : the_row(); ?>
				<div class="site-wide-message">
					<div class="site-wide-message__icon">
						<?php if ( get_sub_field( 'icon_label' ) == 'exclamation' ) { ?>
							<svg fill="none" viewBox="0 0 24 24" stroke="currentColor" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
						<?php } elseif ( get_sub_field( 'icon_label' ) == 'cal' ) { ?>
							<svg fill="none" viewBox="0 0 24 24" stroke="currentColor" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
						<?php } elseif ( get_sub_field( 'icon_label' ) == 'tick' ) { ?>
							<svg fill="none" viewBox="0 0 24 24" stroke="currentColor" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z"></path></svg>
						<?php } elseif ( get_sub_field( 'icon_label' ) == 'book' ) { ?>
							<svg fill="none" viewBox="0 0 24 24" stroke="currentColor" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
						<?php } elseif ( get_sub_field( 'icon_label' ) == 'close' ) { ?>
							<svg fill="none" viewBox="0 0 24 24" stroke="currentColor" ><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"></path></svg>
						<?php } ?>
					</div>

					<div class="site-wide-message__content">
						<h4 class="mb-sm"><?php the_sub_field( 'heading' ); ?></h4>
						<p class="copy-sm mb-sm"><?php the_sub_field( 'context_info' ); ?></p>
						<a href="<?php the_sub_field( 'link' ); ?>" class="text-link mb-0"><?php the_sub_field( 'link_label' ); ?></a>
		
						<button class="site-wide-message__hide">
							<svg viewBox="0 0 24 24"><use xlink:href="#i-close" /></svg>
						</button>
						
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>

	<?php } ?>

<?php } else { ?>

	<div class="no-message"></div>

<?php } ?>