

<?php if ( have_rows( 'menu_items_secondary', 'option' ) ) : ?>
	<nav class="header__nav-secondary">
    	<ul class="list-inline">
			<?php while ( have_rows( 'menu_items_secondary', 'option' ) ) : the_row(); ?>
				<li class="secondary-link"><a href="<?php the_sub_field( 'what_page_should_this_link_to' ); ?>"><?php the_sub_field( 'menu_label' ); ?></a></li>
			<?php endwhile; ?>


<?php if ( have_rows( 'useful_links', 'option' ) ) : ?>

	<li class="useful-links__trigger">
		<a href="#">
			<?php the_field( 'link_label', 'option' ); ?>
			<svg viewBox="0 0 29 29" class=""><use xlink:href="#i-chevron" /></svg>
		</a>
		<ul class="useful-links__menu list-bare">
			<?php while ( have_rows( 'useful_links', 'option' ) ) : the_row(); ?>
				<li><a href="<?php the_sub_field( 'link_url' ); ?>" <?php if ( get_sub_field( 'external_site' ) == true ) { echo "target='_blank'"; } ?>><?php the_sub_field( 'menu_label' ); ?></a></li>
			<?php endwhile; ?>
		</ul>
	</li>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>
		</ul>
	</nav>
<?php else : ?>
	<?php // no rows found ?>
<?php endif; ?>