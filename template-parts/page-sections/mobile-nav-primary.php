<?php $current_page_id = get_the_ID(); ?>


        <?php if ( have_rows( 'mobile_menu_items', 'option' ) ) : ?>
            <?php while ( have_rows( 'mobile_menu_items', 'option' ) ) : the_row(); ?>

                <?php if ( get_sub_field('link_type') == 'link' ) { ?>

                    <?php $post_object = get_sub_field( 'what_page_should_this_link_to' ); ?>
                    <?php if ( $post_object ): ?>
                        <?php $post = $post_object; ?>
                        <?php setup_postdata( $post ); ?> 
                             <li class="<?php if ( get_the_ID() == $current_page_id ) { echo 'current'; } ?>"><a href="<?php the_permalink(); ?>"><?php the_sub_field( 'menu_label' ); ?></a></li>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                <?php } else { ?>

                    <li class="has-sub-menu">
                        <a href="#">
                            <?php the_sub_field( 'menu_label' ); ?>
                            <svg viewBox="0 0 29 29" class=""><use xlink:href="#i-chevron" /></svg>
                        </a>

                        <ul class="sub-menu">
            
                            <?php $post_objects = get_sub_field( 'links' ); ?>
                            <?php if ( $post_objects ): ?>
                                <?php foreach ( $post_objects as $post ):  ?>
                                    <?php setup_postdata( $post ); ?>
                                    <li class="<?php if ( get_the_ID() == $current_page_id ) { echo 'current'; } ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                       

                        </ul>
                    </li>

                <?php } ?>
                
            <?php endwhile; ?>
        <?php else : ?>
            <?php // no rows found ?>
        <?php endif; ?>

