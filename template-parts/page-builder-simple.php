<?php if ( have_rows( 'builder-content' ) ): ?>
	<?php while ( have_rows( 'builder-content' ) ) : the_row(); ?>
		<?php if ( get_row_layout() == 'text_content' ) : ?>

			<div class="section-content">
				<div class="wrapper--small wrapper--z content-editor">
					<?php the_sub_field( 'content' ); ?>
				</div>
			</div>

		<?php elseif ( get_row_layout() == 'media' ) : ?>

			<div class="mb-xl wrapper--z">
				<?php get_template_part( 'template-parts/page-elements/media' ); ?>
			</div>

		<?php elseif ( get_row_layout() == 'accordion' ) : ?>

			<div class="section-content ">
				<div class="wrapper--small wrapper--z">

				<?php if( get_sub_field('heading_optional') ): ?>
					<h3 class="heading-4 mb-lg"><?php the_sub_field( 'heading_optional' ); ?></h3>
				<?php endif; ?>

			<?php if ( have_rows( 'accordion' ) ) : ?>
				<?php
					$accordionID =  uniqid();
				?>
				<div class="accordion" id="a-<?php echo $accordionID; ?>">
					<?php while ( have_rows( 'accordion' ) ) : the_row(); ?>

						<h6 data-toggle="collapse" data-target="#collapse-<?php echo get_row_index(); ?>-<?php echo $accordionID; ?>" aria-expanded="false" aria-controls="collapse-<?php echo get_row_index(); ?>-<?php echo $accordionID; ?>" class="collapsed mb-0 accordion__trigger" >
							<?php the_sub_field( 'heading' ); ?>
							<svg viewBox="0 0 39 39" class=""><use xlink:href="#i-chevron-circle" /></svg>
						</h6>

						<div id="collapse-<?php echo get_row_index(); ?>-<?php echo $accordionID; ?>" class="collapse accordion__target" data-parent="#a-<?php echo $accordionID; ?>">

							<div class="accordion__inner">


								<?php if ( have_rows( 'content' ) ): ?>
									<?php while ( have_rows( 'content' ) ) : the_row(); ?>
										<?php if ( get_row_layout() == 'text_content' ) : ?>

											<div class="content-editor">
												<?php the_sub_field( 'content' ); ?>
											</div>

										<?php elseif ( get_row_layout() == 'links' ) : ?>

											<?php get_template_part( 'template-parts/page-builder-sections/links' ); ?>

										<?php endif; ?>
									<?php endwhile; ?>
								<?php else: ?>
									<?php // no layouts found ?>
								<?php endif; ?>



							</div>





						</div>

					<?php endwhile; ?>
				</div><!-- End accordion -->
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
		</div>
			</div>


	<?php elseif ( get_row_layout() == 'links' ) : ?>
		<div class="section-content">
			<div class="wrapper--small wrapper--z">
				<?php if( get_sub_field('heading_optional') ): ?>
					<h3 class="heading-4 mb-lg"><?php the_sub_field( 'heading_optional' ); ?></h3>
				<?php endif; ?>
			<?php get_template_part( 'template-parts/page-builder-sections/links' ); ?>
			</div>
		</div>

	<?php elseif ( get_row_layout() == 'people' ) : ?>

		<div class="section-content">
			<?php if( get_sub_field('heading_optional') ): ?>
				<h3 class="heading-4 mb-lg"><?php the_sub_field( 'heading_optional' ); ?></h3>
			<?php endif; ?>

			<div class="people row">

            <?php
            $person_type_ids = get_sub_field( 'person_type' );
            // WP_Query arguments
            $args = array(
                'post_type'              => 'people',
                'posts_per_page'         => '20',
                'order'                  => 'ASC',
                'orderby'                => 'menu_order',
                'tax_query' => array(
                    array(
    					'taxonomy' => 'tax-people-type',
    					'field' => 'term_id',
    					'terms' => $person_type_ids
                    )
				),
            );

            // The Query
            $query = new WP_Query( $args );

            

            // The Loop
            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post(); ?>

                    <?php get_template_part( 'template-parts/page-elements/block-person' ); ?>

                <?php }
            } 
            // Restore original Post Data
            wp_reset_postdata();
            ?>
        	</div>
		</div>


	<?php elseif ( get_row_layout() == 'divider' ) : ?>
	
		<div class="section-content">
			<div class="wrapper--small wrapper--z">
				<hr/>
			</div>
		</div>

	<?php endif; ?>


	<?php endwhile; ?>
<?php else: ?>
	<div class="section-content">
		<div class="wrapper--small wrapper--z">
			<?php the_content(); ?>
		</div>
	</div>
<?php endif; ?>





