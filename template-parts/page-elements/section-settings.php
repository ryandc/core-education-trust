<?php if ( get_sub_field('background_colour') == 'black' ) { ?> section--bg bg--black<?php } elseif ( get_sub_field('background_colour') == 'grey' ) { ?> section--bg bg--light-grey<?php } ?>

<?php if ( get_sub_field('margin_top') == 'minimal' ) { ?> mt-xl<?php } elseif ( get_sub_field('margin_top') == 'none' ) { ?> mt-0<?php } ?>
<?php if ( get_sub_field('margin_bottom') == 'minimal' ) { ?> mb-xl<?php } elseif ( get_sub_field('margin_bottom') == 'none' ) { ?> mb-0<?php } ?>
