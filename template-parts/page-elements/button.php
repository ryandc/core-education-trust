

<?php if ( have_rows( 'button' ) ) : ?>
    <?php while ( have_rows( 'button' ) ) : the_row(); ?>

        <?php get_template_part( 'template-parts/page-elements/link-function' ); ?>     

        <?php if ( get_sub_field( 'button_function' ) == 'page' ) { ?>                    
            class="btn link--icon-chevron
        <?php } elseif ( get_sub_field( 'button_function' ) == 'external' ) { ?>
            class="btn link--icon-external
        <?php } elseif ( get_sub_field( 'button_function' ) == 'anchor' ) { ?>
            class="btn link--icon-anchor
        <?php } elseif ( get_sub_field( 'button_function' ) == 'download' ) { ?>
            class="btn link--icon-download
        <?php } elseif ( get_sub_field( 'button_function' ) == 'email' ) { ?>
           class="btn link--icon-email
        <?php } elseif ( get_sub_field( 'button_function' ) == 'gallery' ) { ?>
            class="btn link--icon-gallery
        <?php } ?>

        <?php if ( get_sub_field( 'button_style' ) == 'secondary' ) { ?>
            <?php if ( get_sub_field( 'button_function' ) == 'call' ) { ?><?php } else { ?>             
            btn--secondary <?php } ?>
        <?php } ?>

        <?php if ( get_sub_field( 'button_function' ) == 'call' ) { } else { ?>             
            "><?php the_sub_field( 'button_text' ); ?></a>
        <?php } ?>


        <?php if ( get_sub_field( 'button_function' ) == 'gallery' ) { ?>

            <?php
                $image_gallery_images = get_sub_field( 'image_gallery' ); 
                $skip = true;

                if ( $image_gallery_images ) :  ?>

                <div class="dn">
                <?php foreach ( $image_gallery_images as $image_gallery_image ):
                    if ($skip) {
                        $skip = false;
                            continue;
                        } ?>
                    <a href="<?php echo $image_gallery_image['sizes']['xlarge']; ?>"
                        data-fancybox="<?php $galleryname = get_sub_field( 'gallery_name' ); echo sanitize_title($galleryname); ?>"
                        data-caption="<?php echo $image_gallery_image['caption']; ?>"
                        data-thumb="<?php echo $image_gallery_image['sizes']['4x2-xxs']; ?>"></a>
                <?php endforeach; ?>
                </div>

            <?php endif; ?>


        <?php } ?>
    
    


    <?php endwhile; ?>
<?php endif; ?>

