<a href="<?php the_permalink(); ?>" class="page-link-blocks__item">
	<div class="page-link-blocks__item-inner" data-aos="fade-up" data-aos-delay="<?php echo get_row_index(); ?>00">
		<div>
			<h3 class="mb-lg"><?php the_title(); ?></h3>
			<p class="copy-sm mb-lg"><?php the_sub_field( 'intro_text' ); ?></p>
			<p class="text-link mb-0">Read more</p>
		</div>
	</div>
	<svg viewBox="0 0 24 24" class="link-arrow"><use xlink:href="#i-link-page" /></svg>
</a>