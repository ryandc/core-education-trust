
<?php
	$deadline = get_field('deadline');
	$deadlineformat = DateTime::createFromFormat('Ymd', $deadline);
?>
<div class=" role mb-grid--sm">
	<a href="<?php the_field('link_to_tes'); ?>" target="_blank"  class="box box--padding">
		<div class="role__content">
			<h3 class="role__title heading-4 mb-sm"><?php the_title(); ?></h3>
			<p class="role__date copy-sm mb-xs"><span class="subtle heading-6 inline-block">Apply by: </span><?php echo $deadlineformat->format('j F Y'); ?></p>

			<?php $location_term = get_field( 'location' ); ?>
			<?php if ( $location_term ): ?>
				<p class="role__school copy-sm  mb-sm "><span class="subtle heading-6 inline-block">Location:  </span><?php echo $location_term->name; ?></p>
			<?php endif; ?>

		
			<p class="text-link mb-0">View role on TES</p>
		</div>



		<svg viewBox="0 0 24 24" class="link-arrow"><use xlink:href="#i-link-page" /></svg>

	</a>
</div>

