

	<div class="box box--padding quote-block tl">

		<blockquote>
			<?php the_sub_field( 'quote' ); ?>

			<h4 class="heading-6 mb-0 subtle"><?php the_sub_field( 'quoter' ); ?></h4>
		</blockquote>

	</div>




