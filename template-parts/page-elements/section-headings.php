<?php if ( get_sub_field( 'add_a_heading_intro' ) == 1 ) { ?>
	<div class="wrapper wrapper--offset wrapper--z mb-xxl">
		<h2 class="<?php the_sub_field( 'section_heading_size' ); ?> mb-lg"><?php the_sub_field( 'section_heading' ); ?></h2>
        <?php if ( get_sub_field( 'section_intro_text' )) { ?>
           <p class="copy-lg mb-0"><?php the_sub_field( 'section_intro_text' ); ?></p>
        <?php } ?>
	</div>

<?php } ?>
