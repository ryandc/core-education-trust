            <?php if ( get_sub_field( 'media_type' ) == 'image' ) { ?>

                <?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
    
            <?php } elseif ( get_sub_field( 'media_type' ) == 'video' ) { ?>

                <div class="video-section">
                    <?php the_sub_field( 'video' ); ?>
                </div>

            <?php } elseif ( get_sub_field( 'media_type' ) == 'gallery' ) { ?>

				<?php $image_gallery_images = get_sub_field( 'image_gallery' ); ?>
				<?php if ( $image_gallery_images ) :  ?>
					<div class="row row-margin--small">
						
					<?php foreach ( $image_gallery_images as $image_gallery_image ): ?>
						<div class="col-xs-6 col-md-4 col-lg-3 mb-sm">
							
							<a href="<?php echo $image_gallery_image['url']; ?>" data-fancybox="gallery-<?php echo get_row_index(); ?>" data-caption="<?php echo $image_gallery_image['caption']; ?>">
								<img src="<?php echo $image_gallery_image['sizes']['thumbnail']; ?>" alt="<?php echo $image_gallery_image['alt']; ?>" />
							</a>
	
						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>

            <?php } ?>

