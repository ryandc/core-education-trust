<div data-aos="fade-right" data-aos-delay="200">
							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Start date</h6>
									<?php the_field( 'start_date' ); ?>
								</div>
							</div>

							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Location</h6>
									<?php $location_term = get_field( 'location' ); ?>
									<?php if ( $location_term ): ?>
										<?php echo $location_term->name; ?>
									<?php endif; ?>
								</div>
							</div>

							<div class="job-role-list top-xs">
									<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Role</h6>
									<?php $role_term = get_field( 'role' ); ?>
									<?php if ( $role_term ): ?>
										<?php echo $role_term->name; ?>
									<?php endif; ?>
								</div>
							</div>


							<?php $subject_term = get_field( 'subject' ); ?>
							<?php if ( $subject_term ): ?>
							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Subject</h6>
			
										<?php echo $subject_term->name; ?>
							
								</div>
							</div>
							<?php endif; ?>


							<?php if ( get_field( 'salary' ) ) { ?>
							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 9a2 2 0 10-4 0v5a2 2 0 01-2 2h6m-6-4h4m8 0a9 9 0 11-18 0 9 9 0 0118 0z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Salary</h6>
									<?php the_field( 'salary' ); ?>
								</div>
							</div>
							<?php } ?>		

							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
								</svg>
								<div>
									<h6 class="subtle mb-0">Working pattern</h6>
									<?php $working_pattern_term = get_field( 'working_pattern' ); ?>
									<?php if ( $working_pattern_term ): ?>
										<?php echo $working_pattern_term->name; ?>
									<?php endif; ?>
								</div>
							</div>		
														
							<hr class="job-role-list-divider" />

							<?php
								$deadline = get_field('deadline');
								$deadlineformat = DateTime::createFromFormat('Ymd', $deadline);

								$interview = get_field('interview_date');
								$interviewformat = DateTime::createFromFormat('Ymd', $interview);
							?>

							<div class="job-role-list top-xs">
								<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
								  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
								</svg>	
		
								<div>
									<h6 class="subtle mb-0">Deadline</h6>
									<p class="mb-0"><?php echo $deadlineformat->format('j F Y'); ?></p>
								</div>
							</div>

							<?php if ( $interview ) { ?>

								<div class="job-role-list top-xs mb-0">


									<svg fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z"></path></svg>

			
									<div>
										<h6 class="subtle mb-0">Interview date</h6>
										<p class="mb"><?php echo $interviewformat->format('j F Y'); ?></p>
									</div>
								</div>

							<?php } ?>

							

							<?php
								$application = get_field( 'application_pdf_type' );
							?>

							<?php if ( $application == 'none' ) { ?>
						
							<?php } elseif ( $application == 'custom' ) { ?>
								<a href="<?php the_field( 'custom_application_form_pdf' ); ?>" class="btn btn--lg db"><?php the_field( 'pdf_button_label', 'option' ); ?></a>
							<?php } elseif ( $application == 'support' ) { ?>
								<a href="<?php the_field( 'support_staff_application_pdf', 'option' ); ?>" class="btn btn--lg db"><?php the_field( 'pdf_button_label', 'option' ); ?></a>
							<?php } else { ?>
								<a href="<?php the_field( 'application_pdf', 'option' ); ?>" class="btn btn--lg db"><?php the_field( 'pdf_button_label', 'option' ); ?></a>
							<?php } ?>
													

		

</div>
