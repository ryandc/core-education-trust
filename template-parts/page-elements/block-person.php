<div class="col-xs col-xs-12 col-md-6 col-lg-4">

<?php if ( get_field( 'profile' ) ) { ?>
	<a href="" class="box box--padding tl people__item wrapper--z">
<?php } else { ?>
	<div class="box box--padding tl people__item wrapper--z">
<?php } ?>

		<div class="people__photo">
			<?php $photo = get_field( 'photo' ); ?>
			<?php if ( $photo ) { ?>
				<?php echo wp_get_attachment_image( $photo, '4x4-xs' ); ?>
			<?php } ?>
		</div>

		<p class="copy-sm mb-xs"><?php the_field( 'job_title' ); ?></p>
    	<h3 class="heading-5"><?php the_title(); ?></h3>

		<?php if ( get_field( 'profile' ) ) { ?>
			<svg viewBox="0 0 24 24" class="link-arrow"><use xlink:href="#i-link-page" /></svg>
		<?php } ?>

<?php if ( get_field( 'profile' ) ) { ?>
	</a>
<?php } else { ?>
	</div>
<?php } ?>

		<div class="people__bio">
			<div class="content-scroll">
				<button class="people__bio-close">
					<svg viewBox="0 0 24 24"><use xlink:href="#i-close" /></svg>
				</button>
				<h3><?php the_title(); ?></h3>
	    		<p class="copy-lg mb-lg"><?php the_field( 'job_title' ); ?></p>
	    		<div class="mb-lg">
					<?php the_field( 'profile' ); ?>
				</div>
				
				<?php $photo = get_field( 'photo' ); ?>
				<?php if ( $photo ) { ?>
					<?php echo wp_get_attachment_image( $photo, '3x4-xs' ); ?>
				<?php } ?>
			
			</div>
		</div>

</div>



