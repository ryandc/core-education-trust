<div class="blog-post blog-post--small mb-xl">


    <a href="<?php the_permalink(); ?>" class="blog-post__image">

        <?php
        $image = get_post_thumbnail_id();

        $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAAqADAAQAAAABAAAAAgAAAADO0J6QAAAAC0lEQVQIHWNgQAcAABIAAYAUyswAAAAASUVORK5CYII=";
        $img_src_1 = wp_get_attachment_image_url( $image, '4x2-xs' );
        $img_src_2 = wp_get_attachment_image_url( $image, '4x2-sm' );
        $img_src_3 = wp_get_attachment_image_url( $image, '4x2-md' );
        $img_src_4 = wp_get_attachment_image_url( $image, '4x2-lg' );
        $img_src_5 = wp_get_attachment_image_url( $image, '4x2-xl' );
        $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
         ?>

        <?php if ( $image ) { ?>

      
            <img
                src="<?php echo $img_src_0; ?>"
                data-srcset="
                    <?php echo $img_src_1; ?> 330w,
                    <?php echo $img_src_2; ?> 450w,
                    <?php echo $img_src_3; ?> 600w,
                    <?php echo $img_src_4; ?> 940w,
                    <?php echo $img_src_5; ?> 1200w"
                data-src="<?php echo $img_src_1; ?>"
                data-sizes="auto"
                class="lazyload"
                alt="<?php echo $img_alt; ?>" />
         
        <?php } else { ?>
            <div class="image image--4x2"></div>
        <?php } ?>

    </a>

    <a href="<?php the_permalink(); ?>" class="blog-post__content box box--padding-lg">

        <p><?php the_time('l j F Y'); ?></p>
        <h2 class="heading-4"><?php the_title(); ?></h2>
        <p class="dn db-lg"><?php the_field( 'page_intro_text' ); ?></p>
        <p class="text-link mb-0">Read updates</p>

        <svg viewBox="0 0 24 24" class="link-arrow"><use xlink:href="#i-link-page" /></svg>

    </a>

</div>


