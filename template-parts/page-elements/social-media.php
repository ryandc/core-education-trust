    <?php if ( get_field( 'facebook_account_url', 'option' ) )  { ?>
        <li><a href="<?php the_field( 'facebook_account_url', 'option' ); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-facebook">
                <use xlink:href="#i-facebook" />
            </svg>
            <span class="clip">Facebook</span>
        </a></li>
    <?php } ?>

    <?php if ( get_field( 'instagram_account_url', 'option' ) )  { ?>
        <li><a href="<?php the_field( 'instagram_account_url', 'option' ); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-instagram">
                <use xlink:href="#i-instagram" />
            </svg>
            <span class="clip">Instagram</span>
        </a></li>
    <?php } ?>

    <?php if ( get_field( 'twitter_account_url', 'option' ) )  { ?>
        <li><a href="<?php the_field( 'twitter_account_url', 'option' ); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-twitter">
                <use xlink:href="#i-twitter" />
            </svg>
            <span class="clip">Twitter  (x.com)</span>
        </a></li>
    <?php } ?>

    <?php if ( get_field( 'linkedin_account_url', 'option' ) )  { ?>
        <li><a href="<?php the_field( 'linkedin_account_url', 'option' ); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-linkedin">
                <use xlink:href="#i-linkedin" />
            </svg>
            <span class="clip">LinkedIn</span>
        </a></li>
    <?php } ?>


    <?php if ( get_field( 'youtube_account_url', 'option' ) )  { ?>
        <li><a href="<?php the_field( 'youtube_account_url', 'option' ); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-youtube">
                <use xlink:href="#i-youtube" />
            </svg>
            <span class="clip">YouTube</span>
        </a></li>
    <?php } ?>

