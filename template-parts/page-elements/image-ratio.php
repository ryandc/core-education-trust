<?php if ( have_rows( 'image' ) ) : ?>
    <?php while ( have_rows( 'image' ) ) : the_row(); ?>

        <?php 

        $image = get_sub_field( 'image' );

        if ( get_sub_field( 'image_ratio' ) == '4x2' ) {

            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAAqADAAQAAAABAAAAAgAAAADO0J6QAAAAC0lEQVQIHWNgQAcAABIAAYAUyswAAAAASUVORK5CYII=";
            $img_src_1 = wp_get_attachment_image_url( $image, '4x2-xs' );
            $img_src_2 = wp_get_attachment_image_url( $image, '4x2-sm' );
            $img_src_3 = wp_get_attachment_image_url( $image, '4x2-md' );
            $img_src_4 = wp_get_attachment_image_url( $image, '4x2-lg' );
            $img_src_5 = wp_get_attachment_image_url( $image, '4x2-xl' );
            $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

        } elseif ( get_sub_field( 'image_ratio' ) == '4x3' ) {

            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAYAAAC09K7GAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAABKADAAQAAAABAAAAAwAAAABUhAAoAAAADElEQVQIHWNgIBkAAAAzAAEVe/f+AAAAAElFTkSuQmCC";
            $img_src_1 = wp_get_attachment_image_url( $image, '4x3-xs' );
            $img_src_2 = wp_get_attachment_image_url( $image, '4x3-sm' );
            $img_src_3 = wp_get_attachment_image_url( $image, '4x3-md' );
            $img_src_4 = wp_get_attachment_image_url( $image, '4x3-lg' );
            $img_src_5 = wp_get_attachment_image_url( $image, '4x3-xl' );
            $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

        } elseif ( get_sub_field( 'image_ratio' ) == '3x4' ) {

            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAECAYAAABLLYUHAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAA6ADAAQAAAABAAAABAAAAADGNqBzAAAADElEQVQIHWNgIB0AAAA0AAHke8VoAAAAAElFTkSuQmCC";
            $img_src_1 = wp_get_attachment_image_url( $image, '3x4-xs' );
            $img_src_2 = wp_get_attachment_image_url( $image, '3x4-sm' );
            $img_src_3 = wp_get_attachment_image_url( $image, '3x4-md' );
            $img_src_4 = wp_get_attachment_image_url( $image, '3x4-lg' );
            $img_src_5 = wp_get_attachment_image_url( $image, '3x4-xl' );
            $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

        } elseif ( get_sub_field( 'image_ratio' ) == '4x4' ) {

            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAABKADAAQAAAABAAAABAAAAADmpNw4AAAADElEQVQIHWNgoBwAAABEAAFFxiNWAAAAAElFTkSuQmCC";
            $img_src_1 = wp_get_attachment_image_url( $image, '4x4-xs' );
            $img_src_2 = wp_get_attachment_image_url( $image, '4x4-sm' );
            $img_src_3 = wp_get_attachment_image_url( $image, '4x4-md' );
            $img_src_4 = wp_get_attachment_image_url( $image, '4x4-lg' );
            $img_src_5 = wp_get_attachment_image_url( $image, '4x4-xl' );
            $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

        }


        if ( get_sub_field( 'image_x_padding' ) == 'small' ) {
            echo "<div class='image--x-small'>";
        } elseif ( get_sub_field( 'image_x_padding' ) == 'large' ) {
            echo "<div class='image--x-large'>";
        } 


         if ( $image ) { ?>

            <?php 

            if ( get_sub_field( 'image_ratio' ) == '4x2' ) { ?>
                <div class="image image--4x2">
            <?php } elseif ( get_sub_field( 'image_ratio' ) == '4x3' ) { ?>
                <div class="image image--4x3">
            <?php } elseif ( get_sub_field( 'image_ratio' ) == '3x4' ) { ?>
                <div class="image image--3x4">
            <?php } elseif ( get_sub_field( 'image_ratio' ) == '4x4' ) { ?>
                <div class="image image--4x4">
            <?php } elseif ( get_sub_field( 'image_ratio' ) == 'none' ) { ?>
                <div class="image">
            <?php } ?>
    
                <?php 
                if ( get_sub_field( 'image_ratio' ) == 'none' ) { ?>
                    <?php echo wp_get_attachment_image( $image, 'full' ); ?>
                <?php } else { ?>

                    <img
                        src="<?php echo $img_src_0; ?>"
                        data-srcset="
                            <?php echo $img_src_1; ?> 330w,
                            <?php echo $img_src_2; ?> 450w,
                            <?php echo $img_src_3; ?> 600w,
                            <?php echo $img_src_4; ?> 940w,
                            <?php echo $img_src_5; ?> 1200w"
                        data-src="<?php echo $img_src_1; ?>"
                        data-sizes="auto"
                        class="lazyload"
                        alt="<?php echo $img_alt; ?>" />

                <?php } ?>

            </div>

        <?php } 

        if ( get_sub_field( 'image_x_padding' ) == 'small' ) {
            echo "</div>";
        } elseif ( get_sub_field( 'image_x_padding' ) == 'large' ) {
            echo "</div >";
        } 

     endwhile; ?>
<?php endif; ?>