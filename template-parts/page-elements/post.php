<?php
$category = get_the_category();
$firstCategory = $category[0]->cat_name; ?>

<a href="<?php the_permalink(); ?>" class="post-block link-block">
	<div class="post-block__image">
	    <span class="post-block__cat btn btn--sm"><?php echo $category[0]->cat_name; ?></span>
	    <?php get_template_part( 'template-parts/page-elements/media-image-3x2-featured' ); ?>
	 </div>
	

	<h3 class="heading-4 mt-md mb-md lh-med">
	    <?php the_title(); ?>
	</h3>
	<p class="subtle mb-0">Posted <?php the_time( 'jS M Y' ); ?></p>
</a>



