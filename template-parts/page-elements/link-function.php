
    <?php if ( get_sub_field( 'button_text' ) ) { ?>

        <?php if ( get_sub_field( 'button_function' ) == 'page' ) { ?>                    
            <a href="<?php the_sub_field( 'button_url_internal' ); ?>" title="Link to page" 
        <?php } elseif ( get_sub_field( 'button_function' ) == 'external' ) { ?>
            <a href="<?php the_sub_field( 'button_url' ); ?>" target="_blank" title="Got to <?php the_sub_field( 'button_url' ); ?>" 

        <?php } elseif ( get_sub_field( 'button_function' ) == 'anchor' ) { ?>
            <a href="#<?php the_sub_field( 'anchor_id' ); ?>" title="Scroll down to section" 

        <?php } elseif ( get_sub_field( 'button_function' ) == 'download' ) { ?>
            <a href="<?php the_sub_field( 'button_file' ); ?>" target="_blank" title="Download file" 
        <?php } elseif ( get_sub_field( 'button_function' ) == 'email' ) { ?>


            <a href="mailto:<?php the_sub_field( 'button_email_address' ); ?>?<?php if ( get_sub_field( 'button_cc_email_addresses' ) ) { ?>cc=<?php the_sub_field( 'button_cc_email_addresses' ); ?>&<?php } ?><?php if ( get_sub_field( 'email_subject' ) ) { ?>subject=<?php the_sub_field( 'email_subject' ); ?>&<?php } ?>" target="" title="Email us" 


        <?php } elseif ( get_sub_field( 'button_function' ) == 'call' ) { ?>
            <p class="fake-link"><?php the_sub_field( 'button_text' ); ?></p> 
        <?php } elseif ( get_sub_field( 'button_function' ) == 'gallery' ) { ?>


            <?php $image_gallery_images = get_sub_field( 'image_gallery' ); ?>
            <?php if ( $image_gallery_images ) :  ?>

                <a data-fancybox="<?php $galleryname = get_sub_field( 'gallery_name' ); echo sanitize_title($galleryname); ?>" data-thumbs='{"autoStart":true, "axis": "x"}' data-caption="<?php echo $image_gallery_images[0]['caption']; ?>" data-thumb="<?php echo $image_gallery_images[0]['sizes']['4x2-xxs']; ?>"
                    href="<?php echo $image_gallery_images[0]['sizes']['xxlarge']; ?>"


            <?php endif; ?>


        <?php } ?>
    
        

    <?php } ?>




