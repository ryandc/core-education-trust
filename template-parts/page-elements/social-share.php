
        <li><a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-facebook">
                <use xlink:href="#i-facebook" />
            </svg>
            <span class="clip">Facebook</span>
        </a></li>

        <li><a href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>&via=COREeducate" target="_blank">
            <svg viewBox="0 0 34 34" class="i-twitter">
                <use xlink:href="#i-twitter" />
            </svg>
            <span class="clip">Twitter (x.com)</span>
        </a></li>

        <li><a href="https://www.linkedin.com/sharing/share-offsite/?url=<?php the_permalink(); ?>" target="_blank">
            <svg viewBox="0 0 34 34" class="i-linkedin">
                <use xlink:href="#i-linkedin" />
            </svg>
            <span class="clip">LinkedIn</span>
        </a></li>

