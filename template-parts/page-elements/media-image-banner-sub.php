
    <?php
    $image = get_sub_field( 'image' );
    $img_src_1 = wp_get_attachment_image_url( $image, 'banner-xs' );
    $img_src_2 = wp_get_attachment_image_url( $image, 'banner-sm' );
    $img_src_3 = wp_get_attachment_image_url( $image, 'banner-md' );
    $img_src_4 = wp_get_attachment_image_url( $image, 'banner-mdlg' );
    $img_src_5 = wp_get_attachment_image_url( $image, 'banner-lg' );
    $img_src_6 = wp_get_attachment_image_url( $image, 'banner-xl' );
    $img_src_7 = wp_get_attachment_image_url( $image, 'banner-xxl' );
    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>

    <?php if ( $image ) { ?>
        <picture>
            <source
                data-srcset="<?php echo $img_src_1; ?>"
                media="(max-width: 350px)" />
            <source
                data-srcset="<?php echo $img_src_2; ?>"
                media="(max-width: 480px)" />
            <source
                data-srcset="<?php echo $img_src_3; ?>"
                media="(max-width: 540px)" />
            <source
                data-srcset="<?php echo $img_src_4; ?>"
                media="(max-width: 1085px)" />
            <source
                data-srcset="<?php echo $img_src_5; ?>"
                media="(max-width: 1400px)" />
            <source
                data-srcset="<?php echo $img_src_6; ?>"
                media="(max-width: 1820px)" />
            <source
                data-srcset="<?php echo $img_src_7; ?>" />
            <img
                data-src="<?php echo $img_src_1; ?>"
                class="lazyload"
                data-sizes="auto"
                alt="<?php echo $img_alt; ?>" />
        </picture>
    <?php } ?>