<?php 
// determine parent of current page

$current_page_id = get_the_ID();

if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}

$children = wp_list_pages( array(
        'title_li'    => '',
        'child_of'    => $parent,
        'echo'        => 0,
        'sort_column'  => 'menu_order',
    ) );


if ($children) {
?>
<div data-aos="fade-right" data-aos-delay="200">
    <h3 class="heading-5 c--grey-light-1">In this section:</h3>
    <ul class="list-bare">
        <li class="<?php if ( $parent == $current_page_id ) { echo 'current_page_item'; } ?>"><a href="<?php echo get_the_permalink($parent); ?>"><?php echo get_the_title($parent); ?></a></li>
        <?php

            echo $children; 
        ?>
    </ul>
</div>
<?php 
} 
?>
