<?php if ( have_rows( 'text_link' ) ) : ?>
    <?php while ( have_rows( 'text_link' ) ) : the_row(); ?>

        <?php get_template_part( 'template-parts/page-elements/link-function' ); ?>     

        <?php if ( get_sub_field( 'button_function' ) == 'page' ) { ?>                    
            class="text-link link--icon-chevron
        <?php } elseif ( get_sub_field( 'button_function' ) == 'external' ) { ?>
            class="text-link link--icon-external

        <?php } elseif ( get_sub_field( 'button_function' ) == 'anchor' ) { ?>
            class="text-link link--icon-anchor

        <?php } elseif ( get_sub_field( 'button_function' ) == 'download' ) { ?>
            class="text-link link--icon-download
        <?php } elseif ( get_sub_field( 'button_function' ) == 'email' ) { ?>
           class="text-link link--icon-email
        <?php } elseif ( get_sub_field( 'button_function' ) == 'gallery' ) { ?>

            class="text-link link--icon-gallery
        <?php } ?>

        <?php if ( get_sub_field( 'button_function' ) == 'call' ) { } else { ?>             
            "><?php the_sub_field( 'button_text' ); ?></a>
        <?php } ?>


    <?php endwhile; ?>
<?php endif; ?>
