
<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section of-h">

	<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>

    <div class="wrapper wrapper--offset">

		<div class="quote-slider wrapper--z">
            <div class="swiper-wrapper">

                <?php if ( have_rows( 'quotes' ) ) : ?>
                    <?php while ( have_rows( 'quotes' ) ) : the_row(); ?>
                        <div class="swiper-slide">
                            <?php get_template_part( 'template-parts/page-elements/block-quote' ); ?>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>

            </div>

<!--             <div class="swiper-button-prev" aria-label="Previous">
                <svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 154.47 153.73"><path fill="#e3e8e1" d="M154.47 153.73l-50.86-76.86L154.47 0H50.86L0 76.87l50.86 76.86h103.61z"/></svg>
            </div>

            <div class="swiper-button-next" aria-label="Next">
                <svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 154.47 153.73"><path fill="#e3e8e1" d="M154.47 153.73l-50.86-76.86L154.47 0H50.86L0 76.87l50.86 76.86h103.61z"/></svg>
            </div> -->

        </div>
	
	</div>
</section>