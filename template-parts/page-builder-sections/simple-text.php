<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section section--sm">

	<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>

	<div class="wrapper wrapper--offset wrapper--z content-editor">

		<?php the_sub_field( 'text_content' ); ?>

	</div>
</section>




