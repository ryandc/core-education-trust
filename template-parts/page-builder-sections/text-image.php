
<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section text-image">

	<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>

	<div class="wrapper wrapper--z">
		<div class="row middle-md <?php if ( get_sub_field( 'alignment' ) == 'top' ) {  echo 'top-lg'; } elseif ( get_sub_field( 'alignment' ) == 'bottom' ) { echo 'bottom-lg'; } ?>">

			<div class="col-xs-12 col-mdlg-5 col-lg-6 col-xl-7 <?php if ( get_sub_field( 'flip_content' ) == 0 ) { echo 'last-lg'; } ?>  text-image__image">
				<?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
			</div>

			<div class="col-xs-12 col-mdlg-7 col-lg-6 col-xl-5 <?php if ( get_sub_field( 'alignment' ) == 'top' ) {  echo 'mt-xl'; } elseif ( get_sub_field( 'alignment' ) == 'bottom' ) { echo 'mb-xl'; } ?>">

				<div class="w-30 centered last-p text-image__content">
					<h3 class="heading-2"><?php the_sub_field( 'heading' ); ?></h3>
					<?php the_sub_field( 'intro_text' ); ?>
					
				</div>
				
			</div>

		</div>
	</div>
</section>
