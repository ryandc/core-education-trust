
<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="page-link-blocks <?php the_sub_field( 'choose_a_gradient_type' ); ?>">
	<div class="row center-xs">	

	<?php while ( have_rows( 'page_links' ) ) : the_row(); ?>
		<?php $post_object = get_sub_field( 'page' ); ?>
		<?php if ( $post_object ): ?>
			<?php $post = $post_object; ?>
			<?php setup_postdata( $post ); ?> 
				<div class="col-xs-12 col-md-6 col-lg-4 mb-grid ">
					<?php get_template_part( 'template-parts/page-elements/block-page-links' ); ?>
				</div>
			<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	<?php endwhile; ?>

	</div>
</section>