<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section--bg-lg of-h">

	<div class="wrapper wrapper--z">




				<?php if ( have_rows( 'timeline_entries' ) ): ?>
					<div class="row">
					<?php while ( have_rows( 'timeline_entries' ) ) : the_row(); ?>

							<?php if ( get_row_layout() == 'year' ) : ?>
								<div class="col-xs col-xs-12 col-lg-12 tc timeline__year">
	
									<h1 class="heading-1 subtle"><?php the_sub_field( 'what_year' ); ?></h1>
								</div>

							<?php elseif ( get_row_layout() == 'milestone' ) : ?>

								<div class="col-xs col-xs-12 col-lg-6 timeline__entry">
									<div class="centered w-27" data-aos="fade-up">
										
										<div class="mb-xl">

											<?php
												$image = get_sub_field( 'image' );
												$img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAADCAYAAAC09K7GAAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAABKADAAQAAAABAAAAAwAAAABUhAAoAAAADElEQVQIHWNgIBkAAAAzAAEVe/f+AAAAAElFTkSuQmCC";
												$img_src_1 = wp_get_attachment_image_url( $image, '4x3-xs' );
												$img_src_2 = wp_get_attachment_image_url( $image, '4x3-sm' );
												$img_src_3 = wp_get_attachment_image_url( $image, '4x3-md' );
												$img_src_4 = wp_get_attachment_image_url( $image, '4x3-lg' );
												$img_src_5 = wp_get_attachment_image_url( $image, '4x3-xl' );
												$img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
											?>
											<?php if ( $image ) { ?>

												<div class="image image--4x3">
													<img
													src="<?php echo $img_src_0; ?>"
													data-srcset="
													<?php echo $img_src_1; ?> 330w,
													<?php echo $img_src_2; ?> 450w,
													<?php echo $img_src_3; ?> 600w,
													<?php echo $img_src_4; ?> 940w,
													<?php echo $img_src_5; ?> 1200w"
													data-src="<?php echo $img_src_1; ?>"
													data-sizes="auto"
													class="lazyload"
													alt="<?php echo $img_alt; ?>" />
												</div>

											<?php } ?>
										</div>

										<h3><?php the_sub_field( 'heading' ); ?></h3>
										<?php the_sub_field( 'further_description' ); ?>

									</div>
								</div>

							<?php endif; ?>
					<?php endwhile; ?>
					</div>
				<?php else: ?>
					<?php // no layouts found ?>
				<?php endif; ?>


	</div>


</section>