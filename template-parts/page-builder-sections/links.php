

		

			<?php if ( have_rows( 'links' ) ) : ?>
				<div class="link-rows">
				<?php while ( have_rows( 'links' ) ) : the_row(); ?>



	                        <?php if ( get_sub_field( 'button_function' ) == 'page' ) { ?>     

	                            <a href="<?php the_sub_field( 'button_url_internal' ); ?>" title="" class="link-rows__link">
	                            	<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-page" /></svg>
	                            	<span><?php the_sub_field( 'button_text' ); ?></span>

	                            </a>

	                        <?php } elseif ( get_sub_field( 'button_function' ) == 'external' ) { ?>

	                            <a href="<?php the_sub_field( 'button_url' ); ?>" target="_blank" title="" class="link-rows__link">
	                        		<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-external" /></svg><span><?php the_sub_field( 'button_text' ); ?></span>

	    
	                      		</a>

	                        <?php } elseif ( get_sub_field( 'button_function' ) == 'download' ) { ?>

	                            <a href="<?php the_sub_field( 'button_file' ); ?>" target="_blank" title="" class="link-rows__link">
	                        		<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-download" /></svg><span><?php the_sub_field( 'button_text' ); ?></span>

	                            </a>

	                        <?php } elseif ( get_sub_field( 'button_function' ) == 'email' ) { ?>

	                            <a href="mailto:<?php the_sub_field( 'button_email_address' ); ?>?cc=<?php the_sub_field( 'button_cc_email_addresses' ); ?>&subject=<?php the_sub_field( 'email_subject' ); ?>&" target="" title="" class="link-rows__link">
	                        		<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-email" /></svg><span><?php the_sub_field( 'button_text' ); ?></span>
		
	                            </a>



	                        <?php } elseif ( get_sub_field( 'button_function' ) == 'call' ) { ?>

	                            <a href="tel:<?php the_sub_field( 'phone_number' ); ?>" target="_blank" title="" class="link-rows__link dn-md">
	                        		<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-phone" /></svg><span><?php the_sub_field( 'button_text' ); ?></span>
	                            </a>

	                            <a class="link-rows__link-fake dn db-md">
	                        		<svg viewBox="0 0 24 24" class="link-rows__pre-icon"><use xlink:href="#i-link-phone" /></svg><span><?php the_sub_field( 'button_text' ); ?></span>
	                            </a>


	                        <?php } ?>
	                         						

					
				<?php endwhile; ?>
				</div>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>


