<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section--bg-lg of-h">
	<div class="wrapper wrapper--z">
		<?php
		  $iframe = get_sub_field('code'); //this is your acf field

		  if($iframe) {
			  echo $iframe;
		  }
		?>


	</div>

		<div class="bg--shape">
			<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst" /></svg>
		</div>

</section>
