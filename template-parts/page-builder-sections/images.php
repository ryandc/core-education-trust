
<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section section--none section--bg-sm section-images of-h">

	<div class=" wrapper--z">

		<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>
		
			<?php if ( have_rows( 'row' ) ): ?>
				<?php while ( have_rows( 'row' ) ) : the_row(); ?>
					<?php if ( get_row_layout() == 'one_image' ) : ?>
						<div class="row row-margin--small">
							<div class="col-xs-12">
								<?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
							</div>
						</div>

					<?php elseif ( get_row_layout() == 'two_images' ) : ?>

						<?php if ( have_rows( 'images' ) ) : ?>
							<div class="row row-margin--small middle-xs">
								<?php while ( have_rows( 'images' ) ) : the_row(); ?>
									<div class="col-xs-6">
										<?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
									</div>
								<?php endwhile; ?>
							</div>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>

					<?php elseif ( get_row_layout() == 'three_images' ) : ?>
						<?php if ( have_rows( 'images' ) ) : ?>
							<div class="row row-margin--small middle-xs">
								<?php while ( have_rows( 'images' ) ) : the_row(); ?>
									<div class="col-xs-4">
										<?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
									</div>
								<?php endwhile; ?>
							</div>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>
					<?php elseif ( get_row_layout() == 'four_images' ) : ?>
						<?php if ( have_rows( 'images' ) ) : ?>
							<div class="row row-margin--small middle-xs">
								<?php while ( have_rows( 'images' ) ) : the_row(); ?>
									<div class="col-xs-3">
										<?php get_template_part( 'template-parts/page-elements/image-ratio' ); ?>
									</div>
								<?php endwhile; ?>
							</div>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php else: ?>
				<?php // no layouts found ?>
			<?php endif; ?>

		
	</div>
</section>