<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section section--sm our-schools">
	<div class="wrapper wrapper--z">
		<div class="tc">
			<h2 class="heading-5 subtle mb-lg">
				<?php the_field( 'our_schools_heading', 'option' ); ?>
			</h2>
		</div>
		<div class="row">
	        <?php
	        // WP_Query arguments
	        $args = array(
	            'post_type'              => 'school',
	            'posts_per_page'         => '99',
	            'order'                  => 'ASC',
				'orderby'                => 'rand'
	        );

	        if ( get_field( 'schools_per_row', 'option' ) == 'four' ) {
	        	$perrow = 'col-xs-6 col-md-4 col-lg-3';
	        } elseif ( get_field( 'schools_per_row', 'option' ) == 'five' ) {
	        	$perrow = 'col-xs-6 col-md-4 col-lg-25';
	        } elseif ( get_field( 'schools_per_row', 'option' ) == 'six' ) {
	        	$perrow = 'col-xs-6 col-md-4 col-lg-3 col-xl-2';
	        };

	        // The Query
	        $query = new WP_Query( $args );

	        // The Loop
	        if ( $query->have_posts() ) {
	            while ( $query->have_posts() ) {
	                $query->the_post(); ?>
	                <div class="<?php echo $perrow; ?> tc our-schools__item">
	                	<a href="<?php the_permalink(); ?>">

                            <?php $logo_colour = get_field( 'logo_colour' ); ?>
                            <?php if ( $logo_colour ) { ?>
                                <?php echo wp_get_attachment_image( $logo_colour, 'full' ); ?>
                            <?php } ?>

	                	</a>
	                </div>
	            <?php }
	        } else { ?>
	            We don't have any schools
	        <?php }

	        // Restore original Post Data
	        wp_reset_postdata();
	        ?>
    	</div>
	</div>
</section>
