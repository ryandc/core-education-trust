<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section text-columns">

	<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>

	<div class="wrapper wrapper--z">

		<div class="row center-xs">
			<div class="col-xs-12 col-lg-10">

				<div class="row">
		 			<?php if ( have_rows( 'text_columns' ) ) : ?>
						<?php while ( have_rows( 'text_columns' ) ) : the_row(); ?>
				
							<div class="col-xs-12 col-mdlg-6 text-columns__item" data-aos="fade-up" data-aos-delay="<?php echo get_row_index(); ?>00">

								<div class="w-27 last-p tl">
									<h3 class="heading-4"><?php the_sub_field( 'heading' ); ?></h3>
									<?php the_sub_field( 'text' ); ?>
								</div>
								
							</div>

						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found ?>
					<?php endif; ?>
				</div>

			</div>
		</div>

	</div>
</section>




