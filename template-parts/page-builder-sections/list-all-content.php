<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<?php
if ( get_sub_field( 'which_content' ) == 'foundation' ) { ?>


	<section class="section--bg-lg of-h">
		<div class="wrapper wrapper--z wrapper--offset mb-xxl">
			<h2 class="heading-short-statement"><?php the_field( 'all_core_projects_heading', 'option' ); ?></h2>
			<p class="copy-lg"><?php the_field( 'all_core_projects_intro', 'option' ); ?></p>
		</div>
		<div class="wrapper wrapper--z">

			<?php
			// WP_Query arguments
			$args = array(
			    'post_type'              => 'coreandco',
			    'posts_per_page'         => '99',
	            'order'                  => 'ASC',
				'orderby'                => 'menu_order'
			);

			// The Query
			$query = new WP_Query( $args );

			// The Loop
			if ( $query->have_posts() ) {
			    while ( $query->have_posts() ) {
			        $query->the_post(); ?>
			        
			        <div class="foundation-project-all mb-xxl">
			        	<?php get_template_part( 'template-parts/page-elements/block-project' ); ?>
			        </div>

			    <?php }
			} else { ?>
			    <h3>We don't have any projects</h3>
			<?php }

			// Restore original Post Data
			wp_reset_postdata();
			?>
	   
		</div>


		<div class="bg--shape-large">
			<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst-white" /></svg>
		</div>

	</section>



<?php } elseif ( get_sub_field( 'which_content' ) == 'jobs' ) { ?>


<section class="section--bg-lg of-h">
	<div class="wrapper wrapper--offset wrapper--z mb-lg">
		<h2 class="heading-short-statement mb-lg"><?php the_field( 'all_job_roles_heading', 'option' ); ?></h2>
		<?php echo do_shortcode('[searchandfilter id="1361"]'); ?>
	</div>
	
		<?php echo do_shortcode('[searchandfilter id="1361" show="results"]'); ?>


	<div class="bg--shape">
		<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst" /></svg>
	</div>

</section>






<?php } elseif ( get_sub_field( 'which_content' ) == 'schools' ) { ?>


<section class="section list-all-schools">
	<div class="wrapper wrapper--z">
		<div class="row">
	        <?php
	        // WP_Query arguments
	        $args = array(
	            'post_type'              => 'school',
	            'posts_per_page'         => '99',
	            'order'                  => 'ASC',
				'orderby'                => 'menu_order'
	        );

	        // The Query
	        $query = new WP_Query( $args );

	        // The Loop
	        if ( $query->have_posts() ) {
	        	$count = 0;
	            while ( $query->have_posts() ) {
	                $query->the_post(); $count++; ?>
	                <div class="col-xs col-xs-12 col-mdlg-6 col-xl-4" data-aos="fade-up" data-aos-delay="<?php echo $count; ?>00">
	                	<a href="<?php the_permalink(); ?>"  class="list-all-schools__item box" style="background-color:<?php the_field( 'primary_colour' ); ?>">

	                		<div>
								<?php $logo_white = get_field( 'logo_white' ); ?>
								<?php if ( $logo_white ) { ?>
									<?php echo wp_get_attachment_image( $logo_white, 'full' ); ?>
								<?php } ?>

								<p class="mb-xl"><?php the_field( 'address_1st_line' ); ?><br/>
								<?php the_field( 'address_city' ); ?></p>
							</div>

							<svg viewBox="0 0 24 24" class="link-arrow"><use xlink:href="#i-link-page" /></svg>
	                		


						    <?php
						    $image = get_post_thumbnail_id();

				            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAABKADAAQAAAABAAAABAAAAADmpNw4AAAADElEQVQIHWNgoBwAAABEAAFFxiNWAAAAAElFTkSuQmCC";
				            $img_src_1 = wp_get_attachment_image_url( $image, '4x4-xs' );
				            $img_src_2 = wp_get_attachment_image_url( $image, '4x4-sm' );
				            $img_src_3 = wp_get_attachment_image_url( $image, '4x4-md' );
				            $img_src_4 = wp_get_attachment_image_url( $image, '4x4-lg' );
				            $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true);
				             ?>

						    <?php if ( $image ) { ?>

						  
					            <img
			                        src="<?php echo $img_src_0; ?>"
			                        data-srcset="
			                            <?php echo $img_src_1; ?> 330w,
			                            <?php echo $img_src_2; ?> 450w,
			                            <?php echo $img_src_3; ?> 600w,
			                            <?php echo $img_src_4; ?> 940w"
			                        data-src="<?php echo $img_src_1; ?>"
			                        data-sizes="auto"
			                        class="lazyload list-all-schools__image"
					                alt="<?php echo $img_alt; ?>" />
						     
						    <?php } ?>

	                	</a>
	                </div>
	            <?php }
	        } else { ?>
	            <h3>We don't have any schools</h3>
	        <?php }

	        // Restore original Post Data
	        wp_reset_postdata();
	        ?>
    	</div>
	</div>
</section>





<?php } ?>