
<?php get_template_part( 'template-parts/page-elements/section-id' ); ?>

<section class="section">

	<?php get_template_part( 'template-parts/page-elements/section-headings' ); ?>

    <div class="wrapper wrapper--offset">

		<div class="people row">

            <?php if ( get_sub_field( 'method' ) == 1 ) { ?>
            
                <?php if ( have_rows( 'people_listing' ) ) : ?>
                    <?php while ( have_rows( 'people_listing' ) ) : the_row(); ?>
                        <?php $post_object = get_sub_field( 'person' ); ?>
                        <?php if ( $post_object ): ?>
                            <?php $post = $post_object; ?>
                            <?php setup_postdata( $post ); ?> 
                                <?php get_template_part( 'template-parts/page-elements/block-person-override' ); ?>
                            <?php wp_reset_postdata(); ?>
                        <?php endif; ?>                     

                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>

            <?php } else { ?>

                <?php
                $person_type_ids = get_sub_field( 'person_type' );
                // WP_Query arguments
                $args = array(
                    'post_type'              => 'people',
                    'posts_per_page'         => '20',
                    'order'                  => 'ASC',
                    'orderby'                => 'menu_order',
                    'tax_query' => array(
                        array(
        					'taxonomy' => 'tax-people-type',
        					'field' => 'term_id',
        					'terms' => $person_type_ids
                        )
    				),
                );

                // The Query
                $query = new WP_Query( $args );

                // The Loop
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post(); ?>
                        
                        <?php get_template_part( 'template-parts/page-elements/block-person' ); ?>

                    <?php }
                } 
                // Restore original Post Data
                wp_reset_postdata();

                ?>

            <?php } ?>

        </div>
	
	</div>
</section>