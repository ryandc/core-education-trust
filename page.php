<?php get_header(); ?>
	
	<?php get_template_part( 'template-parts/page-sections/page-heading' ); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



		<main role="main" id="content">

			<div class="wrapper mb-xl">
				<div class="layout">
					<div class="sidebar sidebar--layout">
						<div class="sidebar__inner">

							<?php get_template_part( 'template-parts/page-elements/side-menu' ); ?>

						</div>
					</div>

					<div class="main">

						<?php if( get_field('page_intro_text') ) { ?>

							<div class="wrapper--small wrapper--z">
								<section class="page-intro" data-aos="fade-up" data-aos-delay="150">
									<p class="copy-lg"><?php the_field('page_intro_text'); ?></p>
								</section>
							</div>

						<?php } ?>

						
							<?php get_template_part( 'template-parts/page-builder-simple' ); ?>
						

						<div class="sidebar--mobile sidebar">
							<hr class="section--sm bg--grad-orange" />
							<?php get_template_part( 'template-parts/page-elements/side-menu' ); ?>
						</div>

						
					</div>
				</div>
			</div>

		</main>

		<?php if ( have_rows( 'page_links' ) ) : ?>

				<div class="page-link-blocks <?php the_field( 'choose_a_gradient_type' ); ?>">
					<div class="row center-xs">	
						<?php while ( have_rows( 'page_links' ) ) : the_row(); ?>
							<?php $post_object = get_sub_field( 'page' ); ?>
							<?php if ( $post_object ): ?>
								<?php $post = $post_object; ?>
								<?php setup_postdata( $post ); ?> 

									<div class="col-xs-12 col-md-6 col-mdlg mb-grid">
										<?php get_template_part( 'template-parts/page-elements/block-page-links' ); ?>
									</div>

								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
			
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>



	<?php endwhile; endif; ?>
	
<?php get_footer(); ?>



