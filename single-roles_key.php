<?php
get_header();
get_template_part( 'template-parts/page-sections/page-heading' );
 ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="wrapper mb-xl">
			<div class="layout">
	

				<main class="main" role="main" id="content">

					<div class="sidebar--mobile sidebar">
						<div class="wrapper--small mb-xl">
							<?php the_field('link_to_tes'); ?>
						</div>	
					</div>




				</main>
			</div>
		</div>





	<?php endwhile; endif; ?>


<?php get_footer(); ?>