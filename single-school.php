<?php
	get_header();
	get_template_part( 'template-parts/page-sections/page-heading' );
	get_template_part( 'template-parts/page-sections/page-intro' );
	get_template_part( 'template-parts/page-sections/page-image' );

	$primary = get_field( 'primary_colour' ); 
	$secondary = get_field( 'secondary_colour' ); 

?>

<style type="text/css">
	h1, .heading-1, h2, .heading-2, h3, .heading-3, h4, .heading-4, h5, .heading-5, .heading-long-statement {
		color: <?php echo $primary; ?>;
	}

	.site-wrapper {
		background-color: <?php echo $secondary; ?>;
	}

	.header-image__landscape-shapes svg {
		fill:<?php echo $primary; ?>;
	}

	.school__contact-info li svg {
		color:<?php echo $primary; ?>;
	}

	.btn {
		background-color: <?php echo $primary; ?>;
	}

</style>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<main role="main" id="content">

			<section class="section section--sm">
				<div class="wrapper wrapper--offset">
					<div class="row">

						<div class="col-xs-12 col-mdlg-4 school__logo">
							<?php $logo_colour = get_field( 'logo_colour' ); ?>
							<?php if ( $logo_colour ) { ?>
								<?php echo wp_get_attachment_image( $logo_colour, 'full' ); ?>
							<?php } ?>
						</div>

						<div class="col-xs-12 col-md-5 col-mdlg-4 mb-lg">
							<h3 class="heading-4 mb-md">Location</h3>
							<?php the_title(); ?><br/>
							<?php the_field( 'address_1st_line' ); ?><br/>
							<?php the_field( 'address_city' ); ?><br/>
							<?php the_field( 'address_county' ); ?><br/>
							<?php the_field( 'address_postcode' ); ?>
						</div>

						<div class="col-xs-12 col-md-7 col-mdlg-4">
							<h3 class="heading-4 mb-md">Contact</h3>

							<ul class="list-bare mb-lg school__contact-info">
								<li>
									<svg xmlns="http://www.w3.org/2000/svg" fill="none" class="icon--sm icon--align-m" viewBox="0 0 24 24" stroke="currentColor">
										<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
									</svg>
									
									<a href="mailto:<?php the_field( 'email_address' ); ?>">Email us</a>
								</li>

								<li>
									<svg xmlns="http://www.w3.org/2000/svg" fill="none" class="icon--sm icon--align-m" viewBox="0 0 24 24" stroke="currentColor">
										<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
									</svg>
									<?php the_field( 'phone_number' ); ?>
								</li>		

								<li>
									<svg viewBox="0 0 34 34" class="i-twitter icon--sm icon--align-m" fill="currentColor">
										<use xlink:href="#i-twitter" />
									</svg>
									<a href="https://twitter.com/<?php the_field( 'twitter_account' ); ?>"><?php the_field( 'twitter_account' ); ?></a>
								</li>
							</ul>

							<a href="<?php the_field( 'website_url' ); ?>" class="btn">Visit website</a>
						</div>

					</div>
				</div>
			</section>

			<?php get_template_part( 'template-parts/page-builder' ); ?>

		</main>

		<div class="wrapper">
			<hr/>
		</div>


		<section class="section  section--sm">
			<div class="wrapper">
				<div class="tc">
					<h2 class="heading-5 subtle">
						Our other schools
					</h2>
				</div>
				<div class="row center-xs">


			        <?php
			        $currentID = get_the_ID();
			        // WP_Query arguments
			        $args = array(
			            'post_type'              => 'school',
			            'posts_per_page'         => '99',
			            'post__not_in' => array($currentID),
			            'order'                  => 'DESC',
						'orderby'                => 'menu_order'
			        );

			        if ( get_field( 'schools_per_row', 'option' ) == 'four' ) {
			        	$perrow = 'col-xs-6 col-md-4 col-lg-3';
			        } elseif ( get_field( 'schools_per_row', 'option' ) == 'five' ) {
			        	$perrow = 'col-xs-6 col-md-4 col-lg-25';
			        } elseif ( get_field( 'schools_per_row', 'option' ) == 'six' ) {
			        	$perrow = 'col-xs-6 col-md-4 col-lg-3 col-xl-2';
			        };

			        // The Query
			        $query = new WP_Query( $args );

			        // The Loop
			        if ( $query->have_posts() ) {
			            while ( $query->have_posts() ) {
			                $query->the_post(); ?>
			                <div class="<?php echo $perrow; ?> tc our-schools__item">
			                	<a href="<?php the_permalink(); ?>">

		                            <?php $logo_colour = get_field( 'logo_colour' ); ?>
		                            <?php if ( $logo_colour ) { ?>
		                                <?php echo wp_get_attachment_image( $logo_colour, 'full' ); ?>
		                            <?php } ?>

			                	</a>
			                </div>
			            <?php }
			        } else { ?>
			            We don't have any schools
			        <?php }

			        // Restore original Post Data
			        wp_reset_postdata();
	        		?>

		    	</div>
			</div>
		</section>


	<?php endwhile; endif; ?>


<?php get_footer(); ?>