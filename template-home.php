<?php
/*
 Template Name: Home
*/
get_header();
?>



	<div class="swiper-container home-slider">
		<div class="swiper-wrapper">
			
			<?php if ( have_rows( 'home_slider' ) ) : ?>
				<?php while ( have_rows( 'home_slider' ) ) : the_row(); ?>
					<div class="swiper-slide">

						<div class="home-slider__content">
							<h2 class="<?php the_sub_field( 'heading_size' ); ?> mb-md"><?php the_sub_field( 'heading' ); ?></h2>
							<p><?php the_sub_field( 'extra_info_text' ); ?></p>
							<a href="<?php the_sub_field( 'link' ); ?>" class="text-link"><?php the_sub_field( 'link_label' ); ?></a>
						</div>

						<div class="home-slider__image">
							<?php $image = get_sub_field( 'image' ); ?>
							<?php if ( $image ) { ?>

								<div class="header-image__grad bg--grad-white-fade"></div>

							    <?php
					            $img_src_0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAATCAYAAACHrr18AAAABGdBTUEAALGPC/xhBQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAHqADAAQAAAABAAAAEwAAAAARGdGOAAAAIElEQVRIDWNgGAWjITAaAqMhMBoCoyEwGgKjITBiQgAACPsAAVJMz/cAAAAASUVORK5CYII=";
					            $img_src_1 = wp_get_attachment_image_url( $image, 'header-slider-xs' );
					            $img_src_2 = wp_get_attachment_image_url( $image, 'header-slider-sm' );
					            $img_src_3 = wp_get_attachment_image_url( $image, 'header-slider-md' );
					            $img_src_4 = wp_get_attachment_image_url( $image, 'header-slider-lg' );
					            $img_src_5 = wp_get_attachment_image_url( $image, 'header-slider-xl' );
					            $img_src_6 = wp_get_attachment_image_url( $image, 'header-slider-xxl' );
					            $img_src_7 = wp_get_attachment_image_url( $image, 'header-slider-xxxl' );
							    $img_alt = get_post_meta( $image, '_wp_attachment_image_alt', true); ?>
		
						        <div class="image">
						            <img
				                        src="<?php echo $img_src_0; ?>"
				                        data-srcset="
				                            <?php echo $img_src_1; ?> 330w,
				                            <?php echo $img_src_2; ?> 450w,
				                            <?php echo $img_src_3; ?> 600w,
				                            <?php echo $img_src_4; ?> 940w,
				                            <?php echo $img_src_5; ?> 1200w,
				                            <?php echo $img_src_6; ?> 1800w,
				                            <?php echo $img_src_7; ?> 2400w"
				                        data-src="<?php echo $img_src_1; ?>"
				                        data-sizes="auto"
				                        class="lazyload"
						                alt="<?php echo $img_alt; ?>" />
						        </div>
								
							<?php } ?>
						</div>
					</div>

				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>

		</div>
		<!-- Add Arrows -->
		<div class="home-slider__nav">
			<div class="home-slider__prev"><svg viewBox="0 0 39 39" class=""><use xlink:href="#i-chevron-circle" /></svg></div>
			<div class="home-slider__next"><svg viewBox="0 0 39 39" class=""><use xlink:href="#i-chevron-circle" /></svg></div>
		</div>
		<div class="home-slider__shapes">
			<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst" /></svg>
		</div>

		<div class="home-slider__arrow-down">
			<svg viewBox="0 0 24 24" class=""><use xlink:href="#i-link-page" /></svg>
		</div>

	</div>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<main role="main" id="content" data-aos="fade-up" data-aos-delay="150">

			<?php get_template_part( 'template-parts/page-builder' ); ?>

		</main>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>