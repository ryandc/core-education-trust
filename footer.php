	<footer class="footer">

		<?php if ( get_field( 'footer_award_banner', 'option' ) == 1 ) { ?>

			<style type="text/css">
				.footer__award-banner {
					background: linear-gradient(211deg, <?php the_field( 'background_colour_1', 'option' ); ?> 0%, <?php the_field( 'background_colour_2', 'option' ); ?> 100%);
				}

				.footer__award-banner .heading-3,
				.footer__award-banner .heading-5 {
					color: <?php the_field( 'text_colour', 'option' ); ?>;
				}
				
			</style>

			<section class="footer__award-banner">
				<div class="wrapper wrapper--offset">
		
					<div class="row middle-xs">
						<div class="col-xs-4 col-md-4 col-lg-3 footer__award-banner-image">
							<?php $logoimage = get_field( 'logoimage', 'option' ); ?>
							<?php if ( $logoimage ) { ?>
								<?php echo wp_get_attachment_image( $logoimage, 'large' ); ?>
							<?php } ?>
						</div>
						<div class="col-xs-12 col-md-8 col-lg-9">
							<h3 class="heading-3 mb-0">
								<?php if( get_field('sub_heading', 'option') ): ?>
									<span class="heading-5 mb-sm"><?php the_field( 'sub_heading', 'option' ); ?></span>
								<?php endif; ?>
							
								<?php the_field( 'heading', 'option' ); ?>
							</h3>
						</div>
					
					</div>
				</div>
			</section>

		<?php } else { ?>
		 <hr class="">
		<?php } ?>



		<div class="wrapper wrapper--offset">
			<div class="footer__content">

				<?php if ( have_rows( 'logos_in_footer', 'option' ) ) : ?>
					<div class="footer__content-logos">
						<div class="row middle-xs">
							<?php while ( have_rows( 'logos_in_footer', 'option' ) ) : the_row(); ?>
								<?php $logo = get_sub_field( 'logo' ); ?>
								<?php if ( $logo ) { ?>

									<div class="col-xs-4 col-lg-3 col-xl-2">
		
										<?php if( get_sub_field('link_url') ): ?>
											<a href="<?php the_sub_field( 'link_url' ); ?>" target="blank">
										<?php endif; ?>

										<?php echo wp_get_attachment_image( $logo, 'full' ); ?>
										<?php if( get_sub_field('link_url') ): ?>
											</a>
										<?php endif; ?>
									</div>
								<?php } ?>
							<?php endwhile; ?>
						</div>
					</div>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
		

				<div class="footer__content-left">
		            <nav class="" role="navigation">
		            	<ul class="list-inline">
							<?php if ( have_rows( 'footer_menu', 'option' ) ) : ?>
								<?php while ( have_rows( 'footer_menu', 'option' ) ) : the_row(); ?>
									<li><a href="<?php the_sub_field( 'page_link' ); ?>"><?php the_sub_field( 'page_label' ); ?></a></li>
								<?php endwhile; ?>
							<?php else : ?>
								<?php // no rows found ?>
							<?php endif; ?>
						</ul>
		            </nav>
				</div>
				<div class="footer__content-right">
		            <ul class="list-inline social-icons">
		            	<?php get_template_part( 'template-parts/page-elements/social-media' ); ?>
		            </ul>
				</div>

				<div class="footer__content-left">
		            <div class="copy-sm">&copy; <?php echo date('Y'); ?> <?php the_field( 'website_footer_text', 'option' ); ?></div>
				</div>
				<div class="footer__content-right">
					<div class="copy-sm"><?php the_field( 'website_credit_text', 'option' ); ?></div>
				</div>

			</div>
		</div>
		<div class="footer__bottom-symbol">

	      <?php if ( 
	            is_page_template( 'template-coreandco.php' ) ||
	            is_single() && is_post_type('coreandco')
	        ) { ?>
	        	<?php get_template_part( 'template-parts/svgs/logo-svg' ); ?>
	        <?php } else { ?>
				<svg viewBox="0 0 346 350" class=""><use xlink:href="#logo-core-mark" /></svg>
			<?php } ?>

		</div>
	</footer>


</div>

<div class="bg-overlay"></div>
<?php if ( get_field( 'apply_large_fixed_background' ) == 1 ) { ?>
	<div class="bg--shape-fixed">
		<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst-white" /></svg>
	</div>
<?php } ?>
<?php get_template_part( 'template-parts/svgs/mask-svgs' ); ?>
<?php wp_footer(); ?>

</body>
</html>






