<?php get_header(); ?>
<section class="page-heading">
	<div class="wrapper wrapper--offset">

		<h1><?php the_archive_title(); ?></h1>

	</div>
</section>


	<main role="main" id="content">

		<div class="section">
			<div class="wrapper">
				
				<?php if (have_posts()) :  while (have_posts()) :  the_post(); ?>

					<?php get_template_part( 'template-parts/page-elements/block-post-full' ); ?>

				<?php endwhile; endif; ?>
				
			</div>
		</div>

	</main>

	<?php page_pagi(); ?>

<?php get_footer(); ?>

