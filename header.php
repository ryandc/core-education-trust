<!doctype html>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js <?php if ( is_user_logged_in() ) { echo 'logged-in'; } ?>"  lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="revisit-after" content="14 days">
    <meta name="robots" content="all">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/svg+xml" href="<?php echo get_template_directory_uri(); ?>/favicon.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#777777">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>


    <link rel="preconnect"
        href="https://fonts.gstatic.com"
        crossorigin />

    <link rel="preload"
        as="style"
        href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@600&family=IBM+Plex+Serif:ital@0;1&display=swap" />

    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@600&family=IBM+Plex+Serif:ital@0;1&display=swap"
        media="print" onload="this.media='all'" />


    <!-- Third party scripts -->
    <?php the_field( 'head_code', 'option' ); ?>

</head>


<body <?php body_class( 'animated fadeIn' ); ?>>
    <div class="site-wrapper">
    <a href="#content" class="skip-link clip" title="">Skip to content</a>

    <?php get_template_part( 'template-parts/svgs/common-svgs' ); ?>

    <?php get_template_part( 'template-parts/page-sections/alert' ); ?>

<div class="header-core-logo" >

  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Go to home page">
        <?php if ( is_page_template( 'template-home.php' ) ) { ?>
            <h1 class="mb-0">
                <?php get_template_part( 'template-parts/svgs/logo-svg' ); ?>
                <span class="clip">CORE Education Trust</span>
            </h1>

        <?php } elseif (
            is_page_template( 'template-coreandco.php' ) ||
            is_single() && is_post_type('coreandco')
        ) { ?>
                <?php get_template_part( 'template-parts/svgs/co-logo-svg' ); ?>


        <?php } else { ?>
            <?php get_template_part( 'template-parts/svgs/logo-svg' ); ?>
        <?php } ?>
    </a>

</div>

<header class="header" role="banner" data-aos="fade-down" data-aos-delay="50">
    <div class="wrapper wrapper--offset">

        <div class="header__secondary">
            <div class="header__breadcrumbs">
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                      yoast_breadcrumb( '<p class="mb-0">','</p>' );
                    }
                ?>
            </div>
            <?php get_template_part( 'template-parts/page-sections/header-nav-secondary' ); ?>
        </div>

        <div class="header__primary">

            <nav class="header__nav-primary">
                <ul class="list-bare main-menu-header">
                    <?php get_template_part( 'template-parts/page-sections/header-nav-primary' ); ?>
                </ul>
            </nav>

        </div>

        <div class="header__menu-btn">
            <button class="header__menu-btn__trigger">
                <span class="hamburger"></span>
                <span class="hamburger-label">
                    <span class="hamburger-label__slide">
                        <span>Menu</span>
                    </span>
                </span>
            </button>
        </div>

    </div>
</header>

<aside class="mobile-menu">
    <div class="mobile-menu__scroll-wrapper">
        <div class="mobile-menu__primary">

               <div class="mobile-menu__menu-btn tr">
                    <button class="mobile-menu__menu-btn__trigger">
                    <span class="hamburger hamburger--close"></span>
                    <span class="hamburger-label">
                        <span class="hamburger-label__slide">
                            <span>Close</span>
                        </span>
                    </span>
                </button>
            </div>

            <nav class="mobile-menu__links" role="navigation" id="mobile-menu-navigation">
                <ul class="list-bare">
                    <?php get_template_part( 'template-parts/page-sections/mobile-nav-primary' ); ?>
                </ul>
            </nav>
        </div>
    </div>

    <div class="footer__bottom-symbol">
        <svg viewBox="0 0 346 350" class=""><use xlink:href="#logo-core-mark" /></svg>
    </div>

</aside>
<!-- <div class="mobile-menu__overlay"></div> -->

