<?php get_header(); ?>

<section class="page-heading wrapper--z">
	<div class="wrapper wrapper--offset" data-aos="fade-left" data-aos-delay="100">

		<h1 class="heading-larger"><?php the_field( 'latest_updates_heading', 'option' ); ?></h1>

	</div>
</section>


<section class="page-intro wrapper--z">
	<div class="wrapper wrapper--offset" data-aos="fade-left" data-aos-delay="100">
		<p class="copy-lg"><?php the_field( 'latest_intro', 'option' ); ?></p>
	</div>
</section>


<main role="main" id="content">



	<?php 
	// get current page we are on. If not set we can assume we are on page 1.
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	// are we on page one?
	if(1 == $paged) {

		$count = 0;
		$count2 = 0;

		?>
		
		
		<?php if (have_posts()) :  while (have_posts()) :  the_post(); $count++; ?>

			<?php if ($count == 1) { ?>

				<div class="section--sm">
					<div class="wrapper wrapper--offset wrapper--z">

					<ul class="social-icons--blog-index social-icons list-bare">
						<?php get_template_part( 'template-parts/page-elements/social-media' ); ?>
					</ul>

						<?php get_template_part( 'template-parts/page-elements/block-post-featured' ); ?>
					</div>
				</div>

				<div class=" section--bg bg--grad-blue twitter-block">
					<div class="wrapper">
						<div class="twitter-block__heading mb-xl">
							<h2 class="heading-3 c--white">Latest from <a href="<?php the_field( 'twitter_account_url', 'option' ); ?>">Twitter</a></h2>
							<a href="<?php the_field( 'twitter_account_url', 'option' ); ?>" target="_blank" class="heading-5">Follow us</a>
						</div>
					
						<div id="twitter-block__list" class="twitter-block__list swiper-container"></div>
					
					</div>
				</div>
			
			<?php } elseif ($count == 2) { ?>

				<div class="section">
					<div class="wrapper wrapper--z">
						<?php get_template_part( 'template-parts/page-elements/block-post-full' ); ?>
		

			<?php } elseif ($count == 8) { ?>


						<?php get_template_part( 'template-parts/page-elements/block-post-full' ); ?>
					</div>
				</div>

			<?php } else { ?>

				<?php get_template_part( 'template-parts/page-elements/block-post-full' ); ?>
	
			<?php } ?>

		<?php endwhile; endif; ?>


	<?php } else { ?>

		<div class="section">
			<div class="wrapper">
				
				<?php if (have_posts()) :  while (have_posts()) :  the_post(); ?>

					<?php get_template_part( 'template-parts/page-elements/block-post-full' ); ?>

				<?php endwhile; endif; ?>
				
			</div>
		</div>

	<?php } ?>


	<?php page_pagi(); ?>


</main>


	<div class="bg--shape-fixed">
		<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst" /></svg>
	</div>




<?php get_footer(); ?>



