
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<section class="page-heading wrapper--z">
	<div class="wrapper wrapper--offset" data-aos="fade-left" data-aos-delay="100">

		<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="mb page-heading__type">View all updates</a>
		<h1 class="heading-long-statement"><?php the_title(); ?></h1>
		<p class="copy">Posted <?php the_time( 'jS M Y' ); ?></p>
		<a href="<?php echo esc_url( get_category_link( $category[0]->term_id ) ); ?>" class=""><?php echo $category[0]->cat_name; ?></a>

	</div>
</section>


<main role="main" id="content">


		<div class="wrapper mb-xl">
			<div class="layout">
					<div class="sidebar sidebar--layout section-content">
						<div class="sidebar__inner">

							<div class="sidebar__block">
								<h3 class="heading-5">Share this post</h3>
								<ul class="list-inline social-icons social-icons--left mt ">
									<?php get_template_part( 'template-parts/page-elements/social-share' ); ?>
								</ul>
							</div>

							<?php
							$post_tags = get_the_tags();

							if ( $post_tags ) { ?>
								<div class="sidebar__block">
								<h3 class="heading-5">Post tags:</h3>
								<ul class="list-inline mt">
						    	<?php foreach( $post_tags as $tag ) { ?>
						    		<li><a href="<?php echo esc_url( get_category_link( $tag->term_taxonomy_id ) ); ?>"><?php echo $tag->name; ?>, </a></li>
						   		<?php } ?>
								</ul></div>
							<?php } ?>


						</div>
					</div>
				<div class="main">
					
					<?php get_template_part( 'template-parts/page-builder-simple' ); ?>
								
				</div>
			</div>
		</div>


		<div class="wrapper wrapper--z"><hr class="bg--grad-orange"></div>

			<?php
			$orig_post = $post;
			global $post;
			$tags = wp_get_post_tags($post->ID);

			if ($tags) { ?>

				<section class="section wrapper--z">
					<div class="wrapper wrapper--offset mb-xxl">
						<h2 class="heading-1">Related<br/> updates</h2>
					</div>
					<div class="wrapper">
						<div class="row end-xs">

						<?php
						$tag_ids = array();
						foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
						$args=array(
							'tag__in' => $tag_ids,
							'post__not_in' => array($post->ID),
							'posts_per_page'=>3, // Number of related posts to display.
							'caller_get_posts'=>1
						);

						$my_query = new wp_query( $args );

						while( $my_query->have_posts() ) {
						$my_query->the_post();
						?>
					                <div class="col-xs col-xs-12 col-mdlg-6 tl">
					                	<?php get_template_part( 'template-parts/page-elements/block-post-small' ); ?>
					                </div>
					        <?php } ?>
				    	</div>
					</div>
				</section>



			<?php } else {
			$post = $orig_post; ?>


				<section class="section wrapper--z">
					<div class="wrapper wrapper--offset mb-xxl">
						<h2 class="heading-1">More<br/> updates</h2>
					</div>
					<div class="wrapper">
						<div class="row end-xs">
					        <?php
					        $currentID = get_the_ID();
					        $args = array(
					            'posts_per_page'         => '2',
					            'post__not_in' => array($currentID),
					        );

					        // The Query
					        $query = new WP_Query( $args );

					        // The Loop
					        if ( $query->have_posts() ) {
					            while ( $query->have_posts() ) {
					                $query->the_post(); ?>
					                <div class="col-xs col-xs-12 col-mdlg-6 tl">
					                	<?php get_template_part( 'template-parts/page-elements/block-post-small' ); ?>
					                </div>
					            <?php }
					        } else { ?>
					            We don't have any news
					        <?php }

					        // Restore original Post Data
					        wp_reset_postdata();
					        ?>
				    	</div>
					</div>
				</section>


			<?php }
			wp_reset_query();
			?>

	
			
		</main>

	<?php endwhile; endif; ?>

<div class="bg--shape-fixed">
	<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst-white" /></svg>
</div>

<?php get_footer(); ?>
