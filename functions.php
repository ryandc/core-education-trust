<?php

if ( ! function_exists( 'setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function setup() {
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'xlarge', 1800, 9999 );
	add_image_size( 'xxlarge', 2000, 9999 );

	add_image_size( 'thumbnail-lg', 200, 200, TRUE );

	// 4 x 3
	add_image_size( '4x3-xs', 328, 246, TRUE );
	add_image_size( '4x3-sm', 452, 339, TRUE );
	add_image_size( '4x3-md', 600, 450, TRUE );
	add_image_size( '4x3-lg', 940, 705, TRUE );
	add_image_size( '4x3-xl', 1200, 900, TRUE );
	add_image_size( '4x3-xxl', 1800, 1350, TRUE );
	add_image_size( '4x3-xxxl', 2400, 1800, TRUE );

	// 3 x 4
	add_image_size( '3x4-xs', 330, 440, TRUE );
	add_image_size( '3x4-sm', 450, 600, TRUE );
	add_image_size( '3x4-md', 600, 800, TRUE );
	add_image_size( '3x4-lg', 939, 1252, TRUE );
	add_image_size( '3x4-xl', 1200, 1600, TRUE );
	add_image_size( '3x4-xxl', 1800, 2400, TRUE );
	add_image_size( '3x4-xxxl', 2400, 3200, TRUE );

	// Squares
	add_image_size( '4x4-xs', 330, 330, TRUE );
	add_image_size( '4x4-sm', 450, 450, TRUE );
	add_image_size( '4x4-md', 600, 600, TRUE );
	add_image_size( '4x4-lg', 940, 940, TRUE );
	add_image_size( '4x4-xl', 1200, 1200, TRUE );
	add_image_size( '4x4-xxl', 1800, 1800, TRUE );
	add_image_size( '4x4-xxxl', 2400, 2400, TRUE );

	// Wide but shallow image
	add_image_size( '4x2-xs', 330, 165, TRUE );
	add_image_size( '4x2-sm', 450, 225, TRUE );
	add_image_size( '4x2-md', 600, 300, TRUE );
	add_image_size( '4x2-lg', 940, 470, TRUE );
	add_image_size( '4x2-xl', 1200, 600, TRUE );
	add_image_size( '4x2-xxl', 1800, 900, TRUE );
	add_image_size( '4x2-xxxl', 2400, 1200, TRUE );




	// Header - Masked + cropped
	add_image_size( 'header-mc-xs', 330, 132, TRUE );
	add_image_size( 'header-mc-sm', 450, 180, TRUE );
	add_image_size( 'header-mc-md', 600, 240, TRUE );
	add_image_size( 'header-mc-lg', 940, 376, TRUE );
	add_image_size( 'header-mc-xl', 1200, 480, TRUE );
	add_image_size( 'header-mc-xxl', 1800, 720, TRUE );
	add_image_size( 'header-mc-xxxl', 2400, 960, TRUE );


	// Header - Masked
	add_image_size( 'header-m-xs', 330, 231, TRUE );
	add_image_size( 'header-m-sm', 450, 315, TRUE );
	add_image_size( 'header-m-md', 600, 420, TRUE );
	add_image_size( 'header-m-lg', 940, 658, TRUE );
	add_image_size( 'header-m-xl', 1200, 840, TRUE );
	add_image_size( 'header-m-xxl', 1800, 1260, TRUE );
	add_image_size( 'header-m-xxxl', 2400, 1680, TRUE );


	// Header - Home slider
	add_image_size( 'header-slider-xs', 330, 209, TRUE );
	add_image_size( 'header-slider-sm', 450, 225, TRUE );
	add_image_size( 'header-slider-md', 600, 380, TRUE );
	add_image_size( 'header-slider-lg', 990, 627, TRUE );
	add_image_size( 'header-slider-xl', 1200, 760, TRUE );
	add_image_size( 'header-slider-xxl', 1800, 1140, TRUE );
	add_image_size( 'header-slider-xxxl', 2400, 1520, TRUE );



	// Background of promo + testimoniol blocks
	// add_image_size( 'banner-xs', 350, 525, TRUE );
	// add_image_size( 'banner-sm', 480, 720, TRUE );
	// add_image_size( 'banner-md', 540, 810, TRUE );
	// add_image_size( 'banner-mdlg', 1085, 589, TRUE );
	// add_image_size( 'banner-lg', 1400, 700, TRUE );
	// add_image_size( 'banner-xl', 1820, 700, TRUE );
	// add_image_size( 'banner-xxl', 2030, 700, TRUE );


}
endif;
add_action( 'after_setup_theme', 'setup' );


/**
 * Disable the Gutenberg editor
 */
add_filter('use_block_editor_for_post', '__return_false');



/**
 * Correct custom post type current classes applciation
 */

add_action('nav_menu_css_class', function ($classes, $item) {
		
	// Getting the current post details
	$post = get_queried_object();
	if (isset($post->post_type)) {
		if ($post->post_type == 'post') {
			$current_post_type_slug = get_permalink(get_option('page_for_posts'));
		} else {
			// Getting the post type of the current post
			$current_post_type = get_post_type_object(get_post_type($post->ID));
			$current_post_type_slug = $current_post_type->rewrite['slug'];
		}
				
		// Getting the URL of the menu item
		$menu_slug = strtolower(trim($item->url));
			
		// If the menu item URL contains the current post types slug add the current-menu-item class
		if (strpos($menu_slug, $current_post_type_slug) !== false) {
			$classes[] = 'current-menu-item';
		}
	}
	// Return the corrected set of classes to be added to the menu item
	return $classes;
}, 10, 2);




/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );





/**
 * Enqueue scripts and styles.
 */
function scripts() {

	wp_enqueue_style( 'cet-style', get_template_directory_uri() . '/dist/css/style.css#asyncload' , array(), null );

	wp_enqueue_script( 'cet-scripts', get_template_directory_uri() . '/dist/js/scripts.js', array(), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'scripts' );






/**
 * Echo id by slug
 * get_id_by_slug('any-page-slug');
 */
function get_id_by_slug($page_slug) {
	$page = get_page_by_path($page_slug);
	if ($page) {
		return $page->ID;
	} else {
		return null;
	}
}


/**
 * Echo parent slug
 */
function the_parent_slug() {
  global $post;
  if($post->post_parent == 0) return '';
  $post_data = get_post($post->post_parent);
  return $post_data->post_name;
}


/**
 * Add SVGs
 */
function add_file_types_to_uploads($file_types){
$new_filetypes = array();
$new_filetypes['svg'] = 'image/svg+xml';
$file_types = array_merge($file_types, $new_filetypes );
return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');




/**
 * Adding menu support
 */
register_nav_menu( 'primary', __( 'Primary Menu', 'cet' ) );


/**
 * Post type check
 */
function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) 
        return true;
    return false;
}



/**
 * Add ACF options page
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
	acf_add_options_sub_page('General');
	acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Footer');
	acf_add_options_sub_page('Section defaults');
	
}





// add_action( 'wp_print_scripts', 'wsds_detect_enqueued_scripts' );
// function wsds_detect_enqueued_scripts() {
// 	global $wp_scripts;
// 	foreach( $wp_scripts->queue as $handle ) :
// 		echo $handle . ' | ';
// 	endforeach;
// }



// add_filter( 'script_loader_tag', 'cet_defer_scripts', 10, 3 );
// function wsds_defer_scripts( $tag, $handle, $src ) {

// 	// The handles of the enqueued scripts we want to defer
// 	$defer_scripts = array( 
// 		'cet-scripts',
// 	);

//     if ( in_array( $handle, $defer_scripts ) ) {
//         return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
//     }
    
//     return $tag;
// } 







/**
 * Load small functions to optimise WP or the admin
 */
require get_template_directory() . '/inc/optimise.php';

/**
 * Load Custom post types
 */
require get_template_directory() . '/inc/cpt.php';

/**
 * Load Pagination
 */
require get_template_directory() . '/inc/pagination.php';

/**
 * Load Breadcrumb mods
 */
// require get_template_directory() . '/inc/breadcrumb.php';

/**
 * Load Nav walker mods
 */
require get_template_directory() . '/inc/nav.php';

