<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>

	<section class="page-heading">
		<div class="wrapper wrapper--offset wrapper--z">
			<h1><?php the_field( '404_heading', 'option' ); ?></h1>
		</div>
	</section>

	<section class="page-intro mb-xxl">
		<div class="wrapper wrapper--offset wrapper--z">
			<p class="copy-lg"><?php the_field( '404_intro_text', 'option' ); ?></p>
			<?php the_field( '404_extra_text', 'option' ); ?>
		</div>
	</section>

<div class="bg--shape-fixed">
	<svg viewBox="0 0 843 774" class=""><use xlink:href="#side-burst-white" /></svg>
</div>

<?php get_footer(); ?>