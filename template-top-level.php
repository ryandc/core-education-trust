<?php
/*
 Template Name: Top Level
*/
get_header();
get_template_part( 'template-parts/page-sections/page-heading' );
get_template_part( 'template-parts/page-sections/page-intro' );
get_template_part( 'template-parts/page-sections/page-image' ); ?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<main role="main" id="content" >

			<?php get_template_part( 'template-parts/page-builder' ); ?>

		</main>

	<?php endwhile; endif; ?>


<?php get_footer(); ?>